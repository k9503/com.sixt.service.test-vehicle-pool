package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({GetAvailableSacVehiclesByLocationHandler.class})
public class GetAvailableSacVehiclesByLocationHandlerTest {

    @Autowired
    private GetAvailableSacVehiclesByLocationHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final String VEHICLE_ID = UUID.randomUUID().toString();
    private static final String VIN = "TESTVIN123456789";
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;

    @Test
    public void handleRequest_Success() throws Exception {
        AvailableVehicle availableVehicle = AvailableVehicle.newBuilder().
                setVehicleId(VEHICLE_ID).
                setVin(VIN).
                setAcrissCode(ACRISS_CODE.name()).
                setVehicleType(VehicleType.SAC_AUTOMATION).build();
        List<AvailableVehicle> availableVehicles = new ArrayList<>();
        availableVehicles.add(availableVehicle);

        GetAvailableSacVehiclesByLocationRequest request = GetAvailableSacVehiclesByLocationRequest.newBuilder()
                .setTestLocation(TestLocation.MUNICH).build();

        when(service.getAvailableSacVehiclesByLocation(TestLocation.MUNICH)).thenReturn(availableVehicles);

        GetAvailableSacVehiclesByLocationResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getAvailableVehiclesList()).isNotEmpty();
        AvailableVehicle foundAvailableVehicle = response.getAvailableVehicles(0);
        assertThat(foundAvailableVehicle.getVehicleId()).isEqualTo(availableVehicle.getVehicleId());
        assertThat(foundAvailableVehicle.getVin()).isEqualTo(availableVehicle.getVin());
        assertThat(foundAvailableVehicle.getAcrissCode()).isEqualTo(availableVehicle.getAcrissCode());

        verify(service, times(1)).getAvailableSacVehiclesByLocation(TestLocation.MUNICH);
    }

    @Test
    public void handleRequest_Fail_NoAvailableSacVehicleFound() throws Exception {
        List<AvailableVehicle> availableVehicles = new ArrayList<>();

        GetAvailableSacVehiclesByLocationRequest request = GetAvailableSacVehiclesByLocationRequest.newBuilder()
                .setTestLocation(TestLocation.MUNICH).build();

        when(service.getAvailableSacVehiclesByLocation(TestLocation.MUNICH)).thenReturn(availableVehicles);

        GetAvailableSacVehiclesByLocationResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getAvailableVehiclesList()).isEmpty();

        verify(service, times(1)).getAvailableSacVehiclesByLocation(TestLocation.MUNICH);
    }

    @Test
    public void handleRequest_Fail_Exception() {
        GetAvailableSacVehiclesByLocationRequest request = GetAvailableSacVehiclesByLocationRequest.newBuilder()
                .setTestLocation(TestLocation.MUNICH).build();

        when(service.getAvailableSacVehiclesByLocation(TestLocation.MUNICH)).thenThrow(new RuntimeException("Some error happened"));

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.InternalServerError);

        verify(service, times(1)).getAvailableSacVehiclesByLocation(TestLocation.MUNICH);
    }

}
