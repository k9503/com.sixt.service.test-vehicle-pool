package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.util.VehiclePoolProvider;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({DeleteVehiclePoolHandler.class})
public class DeleteVehiclePoolHandlerTest {

    @Autowired
    private DeleteVehiclePoolHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_AUTOMATION;
    private static final int BRANCH_ID = 11;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final Tenant TENANT = Tenant.SIXT;
    private static final VehicleFuelType VEHICLE_FUEL_TYPE = VehicleFuelType.GASOLINE;

    @Test
    public void handleRequest_Success() throws Exception {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);

        DeleteVehiclePoolRequest request = DeleteVehiclePoolRequest.newBuilder()
                .setVehiclePoolId(vehiclePool.getId()).build();

        DeleteVehiclePoolResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response).isNotNull();

        verify(service, times(1)).deleteVehiclesFromPool(vehiclePool.getId());
        verify(service, times(1)).deleteVehiclePool(vehiclePool.getId());
    }

    @Test
    public void handleRequest_Fail_Exception() {
        String vehiclePoolId = UUID.randomUUID().toString();

        DeleteVehiclePoolRequest request = DeleteVehiclePoolRequest.newBuilder()
                .setVehiclePoolId(vehiclePoolId).build();

        doThrow(new RuntimeException("Some error happened")).when(service).deleteVehiclePool(vehiclePoolId);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.InternalServerError);

        verify(service, times(1)).deleteVehiclePool(vehiclePoolId);
    }

}
