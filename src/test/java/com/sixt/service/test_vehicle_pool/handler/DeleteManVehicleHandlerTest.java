package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.DeleteManVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.DeleteManVehicleResponse;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;

import java.util.UUID;

import static com.sixt.service.test_vehicle_pool.api.DeleteManVehicleResponse.Error.DELETE_FAILED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({DeleteManVehicleHandler.class})
public class DeleteManVehicleHandlerTest {

    @Autowired
    private DeleteManVehicleHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final String VEHICLE_ID = UUID.randomUUID().toString();

    @Test
    public void handleRequest_Success() throws RpcCallException {
        when(service.deleteManualVehicle(VEHICLE_ID))
                .thenReturn(true);

        DeleteManVehicleRequest request = DeleteManVehicleRequest.newBuilder()
                .setVehicleId(VEHICLE_ID)
                .build();

        DeleteManVehicleResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(DeleteManVehicleResponse.getDefaultInstance());

        verify(service, times(1)).deleteManualVehicle(VEHICLE_ID);
    }

    @Test
    public void handleRequest_DeleteFailed() {
        when(service.deleteManualVehicle(VEHICLE_ID))
                .thenReturn(false);

        DeleteManVehicleRequest request = DeleteManVehicleRequest.newBuilder()
                .setVehicleId(VEHICLE_ID)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BackendError);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(DELETE_FAILED.name());

        verify(service, times(1)).deleteManualVehicle(VEHICLE_ID);
    }

}
