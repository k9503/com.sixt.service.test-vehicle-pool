package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.util.VehiclePoolProvider;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import static com.sixt.service.test_vehicle_pool.api.UpdateVehiclePoolResponse.Error.UPDATE_FAILED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({UpdateVehiclePoolHandler.class})
public class UpdateVehiclePoolHandlerTest {

    @Autowired
    private UpdateVehiclePoolHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_AUTOMATION;
    private static final int BRANCH_ID = 11;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final Tenant TENANT = Tenant.SIXT;
    private static final VehicleFuelType VEHICLE_FUEL_TYPE = VehicleFuelType.GASOLINE;

    @Test
    public void handleRequest_Success() throws Exception {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        UpdateVehiclePoolRequest request = UpdateVehiclePoolRequest.newBuilder()
                .setVehiclePool(vehiclePool)
                .build();

        when(service.updateVehiclePool(vehiclePool)).thenReturn(vehiclePool.getId());
        when(service.getVehiclePool(vehiclePool.getId())).thenReturn(vehiclePool);

        UpdateVehiclePoolResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getVehiclePool().getId()).isEqualTo(vehiclePool.getId());
        assertThat(response.getVehiclePool().getVehicleType()).isEqualTo(vehiclePool.getVehicleType());
        assertThat(response.getVehiclePool().getAcrissCode()).isEqualTo(vehiclePool.getAcrissCode());
        assertThat(response.getVehiclePool().getBranchId()).isEqualTo(vehiclePool.getBranchId());
        assertThat(response.getVehiclePool().getTenant()).isEqualTo(vehiclePool.getTenant());
        assertThat(response.getVehiclePool().getPoolSize()).isEqualTo(vehiclePool.getPoolSize());
        assertThat(response.getVehiclePool().getCleanAfterDays()).isEqualTo(vehiclePool.getCleanAfterDays());
        assertThat(response.getVehiclePool().getFuelType()).isEqualTo(vehiclePool.getFuelType());

        verify(service, times(1)).updateVehiclePool(vehiclePool);
        verify(service, times(1)).getVehiclePool(vehiclePool.getId());
    }

    @Test
    public void handleRequest_Fail_UpdateFailed_CentralPositionMissing() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePoolNoCentralPosition(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        UpdateVehiclePoolRequest request = UpdateVehiclePoolRequest.newBuilder()
                .setVehiclePool(vehiclePool)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);

        verify(service, never()).updateVehiclePool(vehiclePool);
        verify(service, never()).getVehiclePool(vehiclePool.getId());
    }

    @Test
    public void handleRequest_Fail_UpdateFailed_CentralPositionLatMissing() {
        CentralPosition centralPosition = CentralPosition.newBuilder()
                .setLon(11.2F).build();
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                centralPosition, VEHICLE_FUEL_TYPE);
        UpdateVehiclePoolRequest request = UpdateVehiclePoolRequest.newBuilder()
                .setVehiclePool(vehiclePool)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);

        verify(service, never()).updateVehiclePool(vehiclePool);
        verify(service, never()).getVehiclePool(vehiclePool.getId());
    }

    @Test
    public void handleRequest_Fail_UpdateFailed_CentralPositionLonMissing() {
        CentralPosition centralPosition = CentralPosition.newBuilder()
                .setLat(44.2F).build();
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                centralPosition, VEHICLE_FUEL_TYPE);
        UpdateVehiclePoolRequest request = UpdateVehiclePoolRequest.newBuilder()
                .setVehiclePool(vehiclePool)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);

        verify(service, never()).updateVehiclePool(vehiclePool);
        verify(service, never()).getVehiclePool(vehiclePool.getId());
    }

    @Test
    public void handleRequest_Fail_UpdateFailed_Exception() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        UpdateVehiclePoolRequest request = UpdateVehiclePoolRequest.newBuilder()
                .setVehiclePool(vehiclePool)
                .build();

        when(service.updateVehiclePool(vehiclePool)).thenThrow(new RuntimeException("Some error happened"));

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BackendError);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(UPDATE_FAILED.name());

        verify(service, times(1)).updateVehiclePool(vehiclePool);
        verify(service, never()).getVehiclePool(vehiclePool.getId());
    }

    @Test
    public void handleRequest_Fail_UpdateFailed_PoolNotFound() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        UpdateVehiclePoolRequest request = UpdateVehiclePoolRequest.newBuilder()
                .setVehiclePool(vehiclePool)
                .build();

        when(service.updateVehiclePool(vehiclePool)).thenReturn(null);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.ResourceNotFound);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(UPDATE_FAILED.name());

        verify(service, times(1)).updateVehiclePool(vehiclePool);
        verify(service, never()).getVehiclePool(vehiclePool.getId());
    }

    @Test
    public void handleRequest_Fail_GettingUpdatedPoolFailed() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        UpdateVehiclePoolRequest request = UpdateVehiclePoolRequest.newBuilder()
                .setVehiclePool(vehiclePool)
                .build();

        when(service.updateVehiclePool(vehiclePool)).thenReturn(vehiclePool.getId());
        when(service.getVehiclePool(vehiclePool.getId())).thenReturn(null);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BackendError);

        verify(service, times(1)).updateVehiclePool(vehiclePool);
        verify(service, times(1)).getVehiclePool(vehiclePool.getId());
    }

}
