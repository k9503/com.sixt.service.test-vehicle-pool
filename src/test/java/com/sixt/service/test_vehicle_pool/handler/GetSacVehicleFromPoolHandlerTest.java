package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.GetSacVehicleFromPoolRequest;
import com.sixt.service.test_vehicle_pool.api.GetSacVehicleFromPoolResponse;
import com.sixt.service.test_vehicle_pool.api.Tenant;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.domain.OutOfTestVehicleException;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.UUID;

import static com.sixt.service.test_vehicle_pool.api.GetSacVehicleFromPoolResponse.Error.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({GetSacVehicleFromPoolHandler.class})
public class GetSacVehicleFromPoolHandlerTest {

    @Autowired
    private GetSacVehicleFromPoolHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_AUTOMATION;
    private static final String VEHICLE_ID = UUID.randomUUID().toString();
    private static final String VIN = "RANDOMVIN01234567";
    private static final int BRANCH_ID = 0;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();
    private static final Tenant TENANT = Tenant.SIXT;

    @Test
    public void handleRequest_Success() throws Exception {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        String testCaseId = "TestCaseIdSuccess";

        GetSacVehicleFromPoolRequest request = GetSacVehicleFromPoolRequest.newBuilder()
                .setTcId(testCaseId)
                .setTenant(TENANT)
                .build();

        when(service.getVehicleFromPool(eq(VEHICLE_TYPE), eq(testCaseId), eq(TENANT)))
                .thenReturn(testVehicle);
        when(service.getNumAvailableVehiclesInPool(eq(VEHICLE_POOL_ID)))
                .thenReturn(10L);

        GetSacVehicleFromPoolResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getVehicleId()).isEqualTo(testVehicle.getStaticVehicleData().getVehicleId());
        assertThat(response.getVin()).isEqualTo(testVehicle.getStaticVehicleData().getBasic().getVin());

        verify(service, times(1)).getVehicleFromPool(VEHICLE_TYPE, testCaseId, TENANT);
        verify(service, times(1)).getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);
    }

    @Test
    public void handleRequest_Fail_RequestNull() {
        GetSacVehicleFromPoolRequest request = null;

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(BAD_REQUEST.name());

        verify(service, never()).getVehicleFromPool(any(), any(), any());
        verify(service, never()).getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);
    }

    @Test
    public void handleRequest_Fail_InvalidTestCaseId() {
        String testCaseId = "";

        GetSacVehicleFromPoolRequest request = GetSacVehicleFromPoolRequest.newBuilder()
                .setTcId(testCaseId)
                .setTenant(TENANT)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(UNDEFINED_TCID.name());

        verify(service, never()).getVehicleFromPool(any(), any(), any());
        verify(service, never()).getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);
    }

    @Test
    public void handleRequest_Fail_InvalidTenant() {
        String testCaseId = "Test case";

        GetSacVehicleFromPoolRequest request = GetSacVehicleFromPoolRequest.newBuilder()
                .setTcId(testCaseId)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(UNDEFINED_TENANT.name());

        verify(service, never()).getVehicleFromPool(any(), any(), any());
        verify(service, never()).getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);
    }

    @Test
    public void handleRequest_Fail_NoVehicleInPool() {
        String testCaseId = "TestCaseNoVehicleInPool";

        GetSacVehicleFromPoolRequest request = GetSacVehicleFromPoolRequest.newBuilder()
                .setTcId(testCaseId)
                .setTenant(TENANT)
                .build();

        doThrow(new OutOfTestVehicleException()).when(service).getVehicleFromPool(VEHICLE_TYPE, testCaseId, TENANT);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.ResourceNotFound);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(OUT_OF_TEST_VEHICLES.name());

        verify(service, times(1)).getVehicleFromPool(VEHICLE_TYPE, testCaseId, TENANT);
        verify(service, never()).getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);
    }

    @Test
    public void handleRequest_Fail_TestVehicleNull() {
        String testCaseId = "TestCaseNoVehicleInPool";

        GetSacVehicleFromPoolRequest request = GetSacVehicleFromPoolRequest.newBuilder()
                .setTcId(testCaseId)
                .setTenant(TENANT)
                .build();

        when(service.getVehicleFromPool(eq(VEHICLE_TYPE), eq(testCaseId), eq(TENANT)))
                .thenReturn(null);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.InternalServerError);

        verify(service, times(1)).getVehicleFromPool(VEHICLE_TYPE, testCaseId, TENANT);
        verify(service, never()).getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);
    }

}
