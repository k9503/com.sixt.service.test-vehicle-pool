package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.api.AddTelematicManRacVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.AddTelematicManRacVehicleResponse;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.CommonRacVehicleCreator;
import com.sixt.service.test_vehicle_pool.infrastructure.FleetServiceProxy;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;

import java.util.UUID;

import static com.sixt.service.test_vehicle_pool.api.AddTelematicManRacVehicleResponse.Error.INTERNAL_ERROR;
import static com.sixt.service.test_vehicle_pool.api.AddTelematicManRacVehicleResponse.Error.VEHICLE_NOT_FOUND;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({AddTelematicManRacVehicleHandler.class})
public class AddTelematicManRacVehicleHandlerTest {

    @Autowired
    private AddTelematicManRacVehicleHandler handler;

    @MockBean
    private CommonRacVehicleCreator commonRacVehicleCreator;

    @MockBean
    private FleetServiceProxy fleetServiceProxy;

    private String vehicleId;
    private String vin;
    private VehicleV2 vehicleV2;

    @Before
    public void setup() {
        vehicleId = UUID.randomUUID().toString();
        vin = "FAKE1234567890123";
        VehicleV2.Basic vehicleV2Basic = VehicleV2.Basic.newBuilder()
                .setVin(vin).build();
        vehicleV2 = VehicleV2.newBuilder()
                .setBasic(vehicleV2Basic)
                .setVehicleId(vehicleId).build();
    }

    @Test
    public void handleRequest_Success() throws Exception {
        AddTelematicManRacVehicleRequest request = AddTelematicManRacVehicleRequest.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(vehicleV2);

        AddTelematicManRacVehicleResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response).isEqualTo(AddTelematicManRacVehicleResponse.getDefaultInstance());

        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
        verify(commonRacVehicleCreator, times(1)).addTelematicUnit(vehicleId, vin);
    }

    @Test
    public void handleRequest_Fail_VehicleNotFound() {
        AddTelematicManRacVehicleRequest request = AddTelematicManRacVehicleRequest.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(null);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.ResourceNotFound);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(VEHICLE_NOT_FOUND.name());

        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
        verify(commonRacVehicleCreator, never()).addTelematicUnit(vehicleId, vin);
    }

    @Test
    public void handleRequest_Fail_UnexpectedError() {
        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(vehicleV2);

        AddTelematicManRacVehicleRequest request = AddTelematicManRacVehicleRequest.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        doAnswer(invocation -> {
            throw new Exception("Some exception");
        }).when(commonRacVehicleCreator).addTelematicUnit(vehicleId, vin);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.InternalServerError);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(INTERNAL_ERROR.name());

        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
        verify(commonRacVehicleCreator, times(1)).addTelematicUnit(vehicleId, vin);
    }

}
