package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.LookupVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.LookupVehicleResponse;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;


import java.util.UUID;

import static com.sixt.service.test_vehicle_pool.api.LookupVehicleResponse.Error.NOT_FOUND;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({LookupVehicleHandler.class})
public class LookupVehicleHandlerTest {

    @Autowired
    private LookupVehicleHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_AUTOMATION;
    private static final String VEHICLE_ID = UUID.randomUUID().toString();
    private static final String VIN = "RANDOMVIN01234567";
    private static final int BRANCH_ID = 0;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();

    @Test
    public void handleRequest_Success() throws Exception {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setTcId("TestCaseIdSuccess");

        LookupVehicleRequest request = LookupVehicleRequest.newBuilder()
                .setVehicleId(VEHICLE_ID)
                .build();

        when(service.lookupVehicleFromPool(eq(VEHICLE_ID)))
                .thenReturn(testVehicle);

        LookupVehicleResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getVehicleType()).isEqualTo(VEHICLE_TYPE);
        assertThat(response.getVin()).isEqualTo(testVehicle.getStaticVehicleData().getBasic().getVin());
        assertThat(response.getTcId()).isEqualTo(testVehicle.getTcId());
        assertThat(response.getAcrissCode()).isEqualTo(testVehicle.getStaticVehicleData().getBasic().getAcrissCode());
        assertThat(response.getBranchId()).isEqualTo(testVehicle.getDynamicVehicleData().getBranchId());
        assertThat(response.getVehiclePoolId()).isEqualTo(testVehicle.getVehiclePoolId());

        verify(service, times(1)).lookupVehicleFromPool(VEHICLE_ID);
    }

    @Test
    public void handleRequest_Fail_VehicleNotFound() {
        LookupVehicleRequest request = LookupVehicleRequest.newBuilder()
                .setVehicleId(VEHICLE_ID)
                .build();

        when(service.lookupVehicleFromPool(eq(VEHICLE_ID)))
                .thenReturn(null);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.ResourceNotFound);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(NOT_FOUND.name());

        verify(service, times(1)).lookupVehicleFromPool(VEHICLE_ID);
    }

}
