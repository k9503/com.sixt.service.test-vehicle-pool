package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.ClearVehiclePoolRequest;
import com.sixt.service.test_vehicle_pool.api.ClearVehiclePoolResponse;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({ClearVehiclePoolHandler.class})
public class ClearVehiclePoolHandlerTest {

    @Autowired
    private ClearVehiclePoolHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    @Test
    public void handleRequest_Success() throws Exception {
        String vehiclePoolId = UUID.randomUUID().toString();

        ClearVehiclePoolRequest request = ClearVehiclePoolRequest.newBuilder()
                .setVehiclePoolId(vehiclePoolId)
                .build();

        ClearVehiclePoolResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response).isNotNull();

        verify(service, times(1)).clearVehiclePool(vehiclePoolId);
    }

    @Test
    public void handleRequest_Fail_ExceptionWhileCleanup() {
        String vehiclePoolId = UUID.randomUUID().toString();

        ClearVehiclePoolRequest request = ClearVehiclePoolRequest.newBuilder()
                .setVehiclePoolId(vehiclePoolId)
                .build();

        doAnswer(invocation -> {
            throw new Exception("Some exception");
        }).when(service).clearVehiclePool(vehiclePoolId);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.InternalServerError);

        verify(service, times(1)).clearVehiclePool(vehiclePoolId);
    }

}
