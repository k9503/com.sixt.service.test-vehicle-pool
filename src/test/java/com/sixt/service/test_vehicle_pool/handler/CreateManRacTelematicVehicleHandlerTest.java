package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.branch.api.BranchAddress;
import com.sixt.service.branch.api.BranchOuterClass;
import com.sixt.service.test_vehicle_pool.api.CreateManRacTelematicVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.CreateManRacTelematicVehicleResponse;
import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({CreateManRacTelematicVehicleHandler.class})
public class CreateManRacTelematicVehicleHandlerTest {

    @Autowired
    private CreateManRacTelematicVehicleHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final VehicleFuelType VEHICLE_FUEL_TYPE = VehicleFuelType.GASOLINE;
    private static final String LICENSE_PLATE = "M- TS 1000";
    private static final String COUNTRY = "DE";
    private static final int BRANCH_ID = 1;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final boolean WITH_WINTER_TYRES = true;


    @Test
    public void handleRequest_Success() throws Exception {
        String vehicleId = UUID.randomUUID().toString();
        BranchAddress.Country country = BranchAddress.Country.newBuilder()
                .setIso2Code(COUNTRY).build();
        BranchAddress.Address address = BranchAddress.Address.newBuilder()
                .setCountry(country).build();
        BranchOuterClass.BranchObject branchObject = BranchOuterClass.BranchObject.newBuilder()
                .addAddresses(address)
                .build();
        BasicRacVehicle basicRacVehicle = new BasicRacVehicle(vehicleId, 1, branchObject, LICENSE_PLATE);

        when(service.asyncCreateManRacTelematicVehicle(BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE, WITH_WINTER_TYRES))
                .thenReturn(basicRacVehicle);

        CreateManRacTelematicVehicleRequest request = CreateManRacTelematicVehicleRequest.newBuilder()
                .setBranchId(BRANCH_ID)
                .setAcrissCode(ACRISS_CODE.name())
                .setVehicleFuelType(VEHICLE_FUEL_TYPE)
                .setWinterTyres(WITH_WINTER_TYRES)
                .build();

        CreateManRacTelematicVehicleResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getVehicleId()).isEqualTo(vehicleId);
        assertThat(response.getBranchId()).isEqualTo(BRANCH_ID);
        assertThat(response.getAcrissCode()).isEqualTo(ACRISS_CODE.name());
        assertThat(response.getCountry()).isEqualTo(COUNTRY);
        assertThat(response.getLicensePlate()).isEqualTo(LICENSE_PLATE);

        verify(service, times(1)).asyncCreateManRacTelematicVehicle(eq(BRANCH_ID), eq(ACRISS_CODE), eq(VEHICLE_FUEL_TYPE),
                eq(WITH_WINTER_TYRES));
    }

    @Test
    public void handleRequest_Fail_BranchNotSupported() {
        CreateManRacTelematicVehicleRequest request = CreateManRacTelematicVehicleRequest.newBuilder()
                .setBranchId(55)
                .setAcrissCode(ACRISS_CODE.name())
                .setVehicleFuelType(VEHICLE_FUEL_TYPE)
                .setWinterTyres(WITH_WINTER_TYRES)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);

        verify(service, never()).asyncCreateManRacTelematicVehicle(eq(1), eq(ACRISS_CODE), eq(VEHICLE_FUEL_TYPE),
                eq(WITH_WINTER_TYRES));
    }

}
