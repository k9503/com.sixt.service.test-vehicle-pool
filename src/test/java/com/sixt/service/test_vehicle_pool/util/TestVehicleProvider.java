package com.sixt.service.test_vehicle_pool.util;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.api.TestLocation;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.*;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

public class TestVehicleProvider {

    public static TestVehicle getTestVehicle(String vehicleId, String vin, VehicleType vehicleType, int branchId,
                                             AcrissCode acrissCode, String vehiclePoolId) {
        TestVehicle testVehicle = new TestVehicle();
        VehicleV2.Basic basicVehicle = VehicleV2.Basic.newBuilder().
                setVin(vin).
                setInternalNumber(14525917).
                setAcrissCode(acrissCode.name()).build();

        VehicleV2.Technical technical = VehicleV2.Technical.newBuilder().
                setTankVolume("80000").build();

        VehicleV2 staticVehicleData = VehicleV2.newBuilder().
                setVehicleId(vehicleId).
                setBasic(basicVehicle).
                setTechnical(technical).build();
        DynamicVehicleData dynamicVehicleData = new DynamicVehicleData();
        VehiclePosition vehiclePosition = new VehiclePosition(42.33F, 11.33F);
        dynamicVehicleData.setPosition(vehiclePosition);
        dynamicVehicleData.setIgnitionState(VehicleIgnitionState.IGNITION_ON);
        dynamicVehicleData.setFuelLevelTotal(60000);
        dynamicVehicleData.setChargeLevel(30);
        dynamicVehicleData.setOdometer(10000);
        dynamicVehicleData.setBranchId(branchId);

        testVehicle.setStaticVehicleData(staticVehicleData);
        testVehicle.setDynamicVehicleData(dynamicVehicleData);
        testVehicle.setVehicleType(vehicleType);
        testVehicle.setVehiclePoolId(vehiclePoolId);
        testVehicle.setTestLocation(TestLocation.MUNICH.name());

        return testVehicle;
    }

}
