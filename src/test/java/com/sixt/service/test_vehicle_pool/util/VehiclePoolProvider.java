package com.sixt.service.test_vehicle_pool.util;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.UUID;

public class VehiclePoolProvider {

    public static VehiclePool getVehiclePool(Tenant tenant, VehicleType vehicleType, int branchId, AcrissCode acrissCode,
                                             VehicleFuelType vehicleFuelType) {
        CentralPosition centralPosition = CentralPosition.newBuilder()
                .setLat(42.7F)
                .setLon(11.2F).build();

        return VehiclePool.newBuilder()
                .setId(UUID.randomUUID().toString())
                .setTenant(tenant)
                .setVehicleType(vehicleType)
                .setAcrissCode(acrissCode.name())
                .setBranchId(branchId)
                .setPoolSize(50)
                .setCleanAfterDays(2)
                .setCentralPosition(centralPosition)
                .setFuelType(vehicleFuelType).build();
    }

    public static VehiclePool getVehiclePool(Tenant tenant, VehicleType vehicleType, int branchId, AcrissCode acrissCode,
                                             CentralPosition centralPosition, VehicleFuelType vehicleFuelType) {
        return VehiclePool.newBuilder()
                .setId(UUID.randomUUID().toString())
                .setTenant(tenant)
                .setVehicleType(vehicleType)
                .setAcrissCode(acrissCode.name())
                .setBranchId(branchId)
                .setPoolSize(50)
                .setCleanAfterDays(2)
                .setCentralPosition(centralPosition)
                .setFuelType(vehicleFuelType).build();
    }

    public static VehiclePool getVehiclePoolNoCentralPosition(Tenant tenant, VehicleType vehicleType, int branchId,
                                                              AcrissCode acrissCode, VehicleFuelType vehicleFuelType) {
        return VehiclePool.newBuilder()
                .setId(UUID.randomUUID().toString())
                .setTenant(tenant)
                .setVehicleType(vehicleType)
                .setAcrissCode(acrissCode.name())
                .setBranchId(branchId)
                .setPoolSize(50)
                .setCleanAfterDays(2)
                .setFuelType(vehicleFuelType).build();
    }

}
