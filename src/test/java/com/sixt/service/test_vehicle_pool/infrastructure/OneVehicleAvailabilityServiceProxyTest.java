package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.journey.api.JourneyOuterClass.ForceEndJourneyCommand;
import com.sixt.service.one_vehicle_availability.api.EndUsageRequest;
import com.sixt.service.one_vehicle_availability.api.EndUsageResponse;
import com.sixt.service.one_vehicle_availability.api.GetAvailabilityQuery;
import com.sixt.service.one_vehicle_availability.api.GetAvailabilityResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class OneVehicleAvailabilityServiceProxyTest {

    @Mock
    private RpcClient<GetAvailabilityResponse> getAvailabilityRpcClient;

    @Mock
    private RpcClient<EndUsageResponse> endUsageRpcClient;

    private OneVehicleAvailabilityServiceProxy serviceProxy;

    private final String vehicleId = UUID.randomUUID().toString();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        serviceProxy = new OneVehicleAvailabilityServiceProxy(getAvailabilityRpcClient, endUsageRpcClient);
    }

    @Test
    public void getAvailability_Success() throws RpcCallException {
        GetAvailabilityResponse getAvailabilityResponse = GetAvailabilityResponse.newBuilder()
                .setVehicleId(vehicleId).build();

        when(getAvailabilityRpcClient.callSynchronous(any(GetAvailabilityQuery.class), any(OrangeContext.class))).thenReturn(getAvailabilityResponse);

        GetAvailabilityResponse returnedGetAvailabilityResponse = serviceProxy.getAvailability(vehicleId);
        assertThat(returnedGetAvailabilityResponse).isNotNull();
        assertThat(returnedGetAvailabilityResponse.getVehicleId()).isEqualTo(vehicleId);
        verify(getAvailabilityRpcClient, times(1))
                .callSynchronous(any(GetAvailabilityQuery.class), any(OrangeContext.class));
    }

    @Test
    public void getAvailability_Fail_RpcException_CategoryResourceNotFound() throws RpcCallException {
        when(getAvailabilityRpcClient.callSynchronous(any(GetAvailabilityQuery.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.ResourceNotFound, "VehicleNot Found"));

        GetAvailabilityResponse getAvailabilityResponse = serviceProxy.getAvailability(vehicleId);
        assertThat(getAvailabilityResponse).isNull();
        verify(getAvailabilityRpcClient, times(1))
                .callSynchronous(any(GetAvailabilityQuery.class), any(OrangeContext.class));
    }

    @Test
    public void getAvailability_Fail_RpcException_CategoryInternalServerError() throws RpcCallException {
        when(getAvailabilityRpcClient.callSynchronous(any(GetAvailabilityQuery.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        GetAvailabilityResponse getAvailabilityResponse = serviceProxy.getAvailability(vehicleId);
        assertThat(getAvailabilityResponse).isNull();
        verify(getAvailabilityRpcClient, times(1))
                .callSynchronous(any(GetAvailabilityQuery.class), any(OrangeContext.class));
    }


    @Test
    public void endUsage_Success() throws RpcCallException {
        String owner = "OWNER";
        String referenceId = UUID.randomUUID().toString();

        serviceProxy.endUsage(vehicleId, owner, referenceId);

        verify(endUsageRpcClient, times(1))
                .callSynchronous(any(EndUsageRequest.class), any(OrangeContext.class));
    }

    @Test
    public void endUsage_Fail_RpcException() throws RpcCallException {
        String owner = "OWNER";
        String referenceId = UUID.randomUUID().toString();

        when(endUsageRpcClient.callSynchronous(any(ForceEndJourneyCommand.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        serviceProxy.endUsage(vehicleId, owner, referenceId);
        verify(endUsageRpcClient, times(1))
                .callSynchronous(any(EndUsageRequest.class), any(OrangeContext.class));
    }

}
