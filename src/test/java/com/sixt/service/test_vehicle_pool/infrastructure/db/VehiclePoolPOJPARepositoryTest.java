package com.sixt.service.test_vehicle_pool.infrastructure.db;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.domain.VehiclePoolPO;
import com.sixt.service.test_vehicle_pool.util.VehiclePoolProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest()
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@ContextConfiguration(initializers = {VehiclePoolPOJPARepositoryTest.Initializer.class})
public class VehiclePoolPOJPARepositoryTest {

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer();

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_AUTOMATION;
    private static final Tenant TENANT = Tenant.SIXT;
    private static final int BRANCH_ID = 11;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();
    private static final VehicleFuelType VEHICLE_FUEL_TYPE = VehicleFuelType.GASOLINE;

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(applicationContext.getEnvironment());
        }
    }

    @Autowired
    private VehiclePoolPOJPARepository vehiclePoolPOJPARepository;

    @Before
    public void beforeTest() {
        vehiclePoolPOJPARepository.deleteAll();
    }

    @Test
    public void addNewPoolAndSave_Success() {
        VehiclePoolPO vehiclePoolPO = createDefaultVehiclePoolPO();

        assertThat(vehiclePoolPO.getVehicleType()).isEqualTo(VEHICLE_TYPE);
        assertThat(vehiclePoolPO.getId()).isEqualTo(VEHICLE_POOL_ID);
        assertThat(vehiclePoolPO.getAcrissCode()).isEqualTo(ACRISS_CODE.name());
        assertThat(vehiclePoolPO.getPoolSize()).isEqualTo(50);
        assertThat(vehiclePoolPO.getTenant()).isEqualTo(Tenant.SIXT.name());
        assertThat(vehiclePoolPO.getBranchId()).isEqualTo(BRANCH_ID);
        assertThat(vehiclePoolPO.getCleanAfterDays()).isEqualTo(2);
        assertThat(vehiclePoolPO.getCentralPosition().getLat()).isEqualTo(42.7F);
        assertThat(vehiclePoolPO.getCentralPosition().getLon()).isEqualTo(11.2F);
        assertThat(vehiclePoolPO.getVehicleFuelType()).isEqualTo(VEHICLE_FUEL_TYPE);
    }

    @Test
    public void updatePoolAndSave_Success() {
        VehiclePoolPO vehiclePoolPO = createDefaultVehiclePoolPO();

        CentralPosition centralPosition = CentralPosition.newBuilder()
                .setLat(66.0F)
                .setLon(77.0F).build();
        VehiclePool updateVehiclePool = VehiclePool.newBuilder()
                .setId(vehiclePoolPO.getId())
                .setTenant(Tenant.HOCHBAHN)
                .setBranchId(7)
                .setAcrissCode(AcrissCode.ECAE.name())
                .setVehicleType(VehicleType.FASTLANE_AUTOMATION)
                .setPoolSize(8)
                .setCleanAfterDays(28)
                .setCentralPosition(centralPosition)
                .setFuelType(VehicleFuelType.DIESEL).build();
        vehiclePoolPO.updatePool(updateVehiclePool);
        vehiclePoolPOJPARepository.save(vehiclePoolPO);

        Optional<VehiclePoolPO> updatedVehiclePoolPOOptional = vehiclePoolPOJPARepository.findById(VEHICLE_POOL_ID);
        assertThat(updatedVehiclePoolPOOptional.isPresent()).isTrue();
        VehiclePoolPO updatedVehiclePoolPO = updatedVehiclePoolPOOptional.get();

        assertThat(updatedVehiclePoolPO.getId()).isEqualTo(VEHICLE_POOL_ID);
        assertThat(updatedVehiclePoolPO.getVehicleType()).isEqualTo(VehicleType.FASTLANE_AUTOMATION);
        assertThat(updatedVehiclePoolPO.getAcrissCode()).isEqualTo(AcrissCode.ECAE.name());
        assertThat(updatedVehiclePoolPO.getPoolSize()).isEqualTo(8);
        assertThat(updatedVehiclePoolPO.getTenant()).isEqualTo(Tenant.HOCHBAHN.name());
        assertThat(updatedVehiclePoolPO.getBranchId()).isEqualTo(7);
        assertThat(updatedVehiclePoolPO.getCleanAfterDays()).isEqualTo(28);
        assertThat(updatedVehiclePoolPO.getCentralPosition().getLat()).isEqualTo(66.0F);
        assertThat(updatedVehiclePoolPO.getCentralPosition().getLon()).isEqualTo(77.0F);
        assertThat(updatedVehiclePoolPO.getVehicleFuelType()).isEqualTo(VehicleFuelType.DIESEL);
    }

    @Test
    public void findByTenant_Success() {
        createDefaultVehiclePoolPO();

        Optional<VehiclePoolPO> vehiclePoolPOOptional = vehiclePoolPOJPARepository.findByTenantAndVehicleType(TENANT.name(), VEHICLE_TYPE.name());

        assertThat(vehiclePoolPOOptional.isPresent()).isTrue();
        VehiclePoolPO foundVehiclePoolPO = vehiclePoolPOOptional.get();

        assertThat(foundVehiclePoolPO.getId()).isEqualTo(VEHICLE_POOL_ID);
    }

    @Test
    public void delete_Success() {
        VehiclePoolPO vehiclePoolPO = createDefaultVehiclePoolPO();

        vehiclePoolPOJPARepository.delete(vehiclePoolPO);

        Optional<VehiclePoolPO> deletedVehiclePoolPOOptional = vehiclePoolPOJPARepository.findById(VEHICLE_POOL_ID);
        assertThat(deletedVehiclePoolPOOptional.isPresent()).isFalse();
    }

    /**
     * Helper methods
     */

    @NotNull
    private VehiclePoolPO createDefaultVehiclePoolPO() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);

        VehiclePoolPO vehiclePoolPOToCreate = VehiclePoolPO.createNew(VEHICLE_POOL_ID);
        vehiclePoolPOToCreate.newPool(vehiclePool);
        vehiclePoolPOJPARepository.save(vehiclePoolPOToCreate);

        Optional<VehiclePoolPO> vehiclePoolPOOptional = vehiclePoolPOJPARepository.findById(VEHICLE_POOL_ID);

        assertThat(vehiclePoolPOOptional.isPresent()).isTrue();
        return vehiclePoolPOOptional.get();
    }

}
