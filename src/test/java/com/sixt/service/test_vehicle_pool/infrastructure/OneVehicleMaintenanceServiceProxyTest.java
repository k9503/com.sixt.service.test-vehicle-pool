package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.one_vehicle_maintenance.api.ReleaseResponse;
import com.sixt.service.one_vehicle_maintenance.api.ReleaseVehicleRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class OneVehicleMaintenanceServiceProxyTest {

    @Mock
    private RpcClient<ReleaseResponse> releaseVehicleRpcClient;

    private OneVehicleMaintenanceServiceProxy serviceProxy;

    private final String vehicleId = UUID.randomUUID().toString();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        serviceProxy = new OneVehicleMaintenanceServiceProxy(releaseVehicleRpcClient);
    }

    @Test
    public void releaseVehicle_Success() throws RpcCallException {
        serviceProxy.releaseVehicle(vehicleId);

        verify(releaseVehicleRpcClient, times(1))
                .callSynchronous(any(ReleaseVehicleRequest.class), any(OrangeContext.class));
    }

    @Test
    public void releaseVehicle_Fail_RpcException() throws RpcCallException {
        when(releaseVehicleRpcClient.callSynchronous(any(ReleaseVehicleRequest.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        serviceProxy.releaseVehicle(vehicleId);

        verify(releaseVehicleRpcClient, times(1))
                .callSynchronous(any(ReleaseVehicleRequest.class), any(OrangeContext.class));
    }

}
