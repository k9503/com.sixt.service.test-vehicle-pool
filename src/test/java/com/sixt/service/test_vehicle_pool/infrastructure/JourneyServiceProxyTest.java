package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.journey.api.JourneyOuterClass.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class JourneyServiceProxyTest {

    @Mock
    private RpcClient<GetVehicleJourneyResponse> getVehicleJourneyRpcClient;

    @Mock
    private RpcClient<ForceEndJourneyResponse> forceEndJourneyRpcClient;

    private JourneyServiceProxy serviceProxy;

    private final String journeyId = UUID.randomUUID().toString();
    private final String vehicleId = UUID.randomUUID().toString();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        serviceProxy = new JourneyServiceProxy(getVehicleJourneyRpcClient, forceEndJourneyRpcClient);
    }

    @Test
    public void getVehicleJourney_Success() throws RpcCallException {
        GetVehicleJourneyResponse getVehicleJourneyResponse = GetVehicleJourneyResponse.newBuilder()
                .setJourneyId(journeyId).build();

        when(getVehicleJourneyRpcClient.callSynchronous(any(GetVehicleJourneyQuery.class), any(OrangeContext.class))).thenReturn(getVehicleJourneyResponse);

        GetVehicleJourneyResponse returnedGetVehicleJourneyResponse = serviceProxy.getVehicleJourney(vehicleId);
        assertThat(returnedGetVehicleJourneyResponse).isNotNull();
        assertThat(returnedGetVehicleJourneyResponse.getJourneyId()).isEqualTo(journeyId);
        verify(getVehicleJourneyRpcClient, times(1))
                .callSynchronous(any(GetVehicleJourneyQuery.class), any(OrangeContext.class));
    }

    @Test
    public void getVehicleJourney_Fail_RpcException_NoCurrentJourney() throws RpcCallException {
        when(getVehicleJourneyRpcClient.callSynchronous(any(GetVehicleJourneyQuery.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "No current journey found").
                        withErrorCode(GetVehicleJourneyResponse.Error.NO_CURRENT_JOURNEY.toString()));

        GetVehicleJourneyResponse getVehicleJourneyResponse = serviceProxy.getVehicleJourney(vehicleId);
        assertThat(getVehicleJourneyResponse).isNull();
        verify(getVehicleJourneyRpcClient, times(1))
                .callSynchronous(any(GetVehicleJourneyQuery.class), any(OrangeContext.class));
    }

    @Test
    public void getVehicleJourney_Fail_RpcException_OtherErrorCode() throws RpcCallException {
        when(getVehicleJourneyRpcClient.callSynchronous(any(GetVehicleJourneyQuery.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Invalid vehicle id").
                        withErrorCode(GetVehicleJourneyResponse.Error.INVALID_VEHICLE_ID.toString()));

        GetVehicleJourneyResponse getVehicleJourneyResponse = serviceProxy.getVehicleJourney(vehicleId);
        assertThat(getVehicleJourneyResponse).isNull();
        verify(getVehicleJourneyRpcClient, times(1))
                .callSynchronous(any(GetVehicleJourneyQuery.class), any(OrangeContext.class));
    }

    @Test
    public void forceEndJourney_Success() throws RpcCallException {
        serviceProxy.forceEndJourney(journeyId);

        verify(forceEndJourneyRpcClient, times(1))
                .callSynchronous(any(ForceEndJourneyCommand.class), any(OrangeContext.class));
    }

    @Test
    public void forceEndJourney_Fail_RpcException() throws RpcCallException {
        when(forceEndJourneyRpcClient.callSynchronous(any(ForceEndJourneyCommand.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        serviceProxy.forceEndJourney(journeyId);
        verify(forceEndJourneyRpcClient, times(1))
                .callSynchronous(any(ForceEndJourneyCommand.class), any(OrangeContext.class));
    }

}
