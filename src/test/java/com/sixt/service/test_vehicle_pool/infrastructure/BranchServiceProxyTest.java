package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.branch.api.BranchOuterClass.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class BranchServiceProxyTest {

    @Mock
    private RpcClient<BranchGetResponse> getRpcClient;

    private BranchServiceProxy serviceProxy;

    private int branchId;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        serviceProxy = new BranchServiceProxy(getRpcClient);
        branchId = 11;
    }

    @Test
    public void get_Success() throws RpcCallException {
        BranchObject branchObject = BranchObject.newBuilder()
                .setBranchId(branchId).build();
        BranchGetResponse branchGetResponse = BranchGetResponse.newBuilder()
                .setBranch(branchObject).build();

        when(getRpcClient.callSynchronous(any(BranchGetRequest.class), any(OrangeContext.class))).thenReturn(branchGetResponse);

        BranchObject gotBranchObject = serviceProxy.get(branchId);
        assertThat(gotBranchObject).isNotNull();
        assertThat(gotBranchObject.getBranchId()).isEqualTo(branchId);
        verify(getRpcClient, times(1))
                .callSynchronous(any(BranchGetRequest.class), any(OrangeContext.class));
    }

    @Test
    public void get_Fail_RpcException() throws RpcCallException {
        when(getRpcClient.callSynchronous(any(BranchGetRequest.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        BranchObject gotBranchObject = serviceProxy.get(branchId);
        assertThat(gotBranchObject).isNull();
        verify(getRpcClient, times(1))
                .callSynchronous(any(BranchGetRequest.class), any(OrangeContext.class));
    }

}
