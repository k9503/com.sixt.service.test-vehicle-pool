package com.sixt.service.test_vehicle_pool.application.dto;

import com.sixt.service.test_vehicle_pool.api.TestLocation;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RandomVehiclePositionProviderTest {

    private final TestLocation testLocation = TestLocation.MUNICH;

    @Test
    public void createPosition_Success() {
        VehiclePosition vehiclePosition = RandomVehiclePositionProvider.createPosition(testLocation);
        assertThat(vehiclePosition).isNotNull();
        assertThat(vehiclePosition.getLatitude()).isBetween(48.02f, 48.06f);
        assertThat(vehiclePosition.getLongitude()).isBetween(11.49f, 11.53f);
    }

    @Test
    public void getRandomVehiclePosition_Success() {
        VehiclePosition vehiclePosition = RandomVehiclePositionProvider.getRandomVehiclePosition(48.042964F, 11.510944F, 200);
        assertThat(vehiclePosition).isNotNull();
        assertThat(vehiclePosition.getLatitude()).isBetween(48.02f, 48.06f);
        assertThat(vehiclePosition.getLongitude()).isBetween(11.49f, 11.53f);
    }

}
