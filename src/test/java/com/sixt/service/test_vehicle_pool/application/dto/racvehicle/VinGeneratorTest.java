package com.sixt.service.test_vehicle_pool.application.dto.racvehicle;

import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.VinGenerator;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class VinGeneratorTest {

    @Test
    public void generateVin_Success() {
        String vin = VinGenerator.getVinByType(VehicleType.SAC_AUTOMATION);
        assertThat(vin).isNotNull();
        assertThat(vin.length()).isEqualTo(17);
    }

}
