package com.sixt.service.test_vehicle_pool.domain.vehicledeletion;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.journey.api.JourneyOuterClass.GetVehicleJourneyResponse;
import com.sixt.service.journey.api.JourneyOuterClass.PublicJourneyState;
import com.sixt.service.one_vehicle_availability.api.GetAvailabilityResponse;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.EventApi;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.domain.TestVehicleDataCollector;
import com.sixt.service.test_vehicle_pool.infrastructure.*;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({VehicleCleaner.class})
public class VehicleCleanerTest {

    @Autowired
    private VehicleCleaner vehicleCleaner;

    @MockBean
    private FleetServiceProxy fleetServiceProxy;

    @MockBean
    private OneVehicleMaintenanceServiceProxy oneVehicleMaintenanceServiceProxy;

    @MockBean
    private OneVehicleAvailabilityServiceProxy oneVehicleAvailabilityServiceProxy;

    @MockBean
    private JourneyServiceProxy journeyServiceProxy;

    @MockBean
    private EventApi eventApi;

    @MockBean
    private TestVehicleDataCollector testVehicleDataCollector;

    @MockBean
    private RacVehicleDeletionStepsProxy racVehicleDeletionStepsProxy;

    private final String vehicleId = UUID.randomUUID().toString();
    private final String vin = "RANDOMVIN01234567";
    private final String journeyId = UUID.randomUUID().toString();
    private final String vehiclePoolId = UUID.randomUUID().toString();

    @Test
    public void deleteVehicle_SacAutomation_Success() {
        VehicleType vehicleType = VehicleType.SAC_AUTOMATION;
        int branchId = 0;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);

        // Set behaviour for vehicle cleanup before delete -> Cleanup necessary
        setCleanupBehaviour(PublicJourneyState.PJS_DRIVING, "UNAVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);
        when(fleetServiceProxy.deleteVehicle(vehicleId))
                .thenReturn(true);
        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().
                setVin(vin).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().
                setVehicleId(vehicleId).
                setBasic(basic).
                setIsDeleted(true).build();
        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(vehicleV2);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isTrue();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, times(1)).deleteVehicle(vehicleId);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
    }

    @Test
    public void deleteVehicle_RacTelematicAutomation_NoCleanup_Success() {
        VehicleType vehicleType = VehicleType.RAC_TELEMATIC_AUTOMATION;
        int branchId = 11;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);
        when(fleetServiceProxy.deleteVehicle(vehicleId))
                .thenReturn(true);
        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().
                setVin(vin).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().
                setVehicleId(vehicleId).
                setBasic(basic).
                setIsDeleted(true).build();
        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(vehicleV2);
        when(racVehicleDeletionStepsProxy.cancelVehicle(vehicleId))
                .thenReturn(true);
        when(journeyServiceProxy.getVehicleJourney(vehicleId))
                .thenReturn(null);
        when(oneVehicleAvailabilityServiceProxy.getAvailability(vehicleId))
                .thenReturn(null);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isTrue();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, times(1)).deleteVehicle(vehicleId);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
        verify(racVehicleDeletionStepsProxy, times(1)).cancelVehicle(vehicleId);
        verify(journeyServiceProxy, times(1)).getVehicleJourney(vehicleId);
        verify(journeyServiceProxy, never()).forceEndJourney(anyString());
        verify(oneVehicleAvailabilityServiceProxy, times(1)).getAvailability(vehicleId);
        verify(oneVehicleAvailabilityServiceProxy, never()).endUsage(anyString(), anyString(), anyString());
    }

    @Test
    public void deleteVehicle_FastlaneAutomation_NoCleanup_Success() {
        VehicleType vehicleType = VehicleType.FASTLANE_AUTOMATION;
        int branchId = 11;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);
        when(fleetServiceProxy.deleteVehicle(vehicleId))
                .thenReturn(true);
        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().
                setVin(vin).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().
                setVehicleId(vehicleId).
                setBasic(basic).
                setIsDeleted(true).build();
        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(vehicleV2);
        when(racVehicleDeletionStepsProxy.cancelVehicle(vehicleId))
                .thenReturn(true);
        when(journeyServiceProxy.getVehicleJourney(vehicleId))
                .thenReturn(null);
        when(oneVehicleAvailabilityServiceProxy.getAvailability(vehicleId))
                .thenReturn(null);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isTrue();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, times(1)).deleteVehicle(vehicleId);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
        verify(racVehicleDeletionStepsProxy, times(1)).cancelVehicle(vehicleId);
        verify(journeyServiceProxy, times(1)).getVehicleJourney(vehicleId);
        verify(journeyServiceProxy, never()).forceEndJourney(anyString());
        verify(oneVehicleAvailabilityServiceProxy, times(1)).getAvailability(vehicleId);
        verify(oneVehicleAvailabilityServiceProxy, never()).endUsage(anyString(), anyString(), anyString());
    }

    @Test
    public void deleteSacVehicle_CanNotGetCurrentTestVehicleData() {
        VehicleType vehicleType = VehicleType.SAC_AUTOMATION;

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(null);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, never()).getByVehicleIdV2(vehicleId);
        verify(fleetServiceProxy, never()).deleteVehicle(vehicleId);
    }

    @Test
    public void deleteSacVehicle_CanNotGetStaticVehicleData() {
        VehicleType vehicleType = VehicleType.SAC_AUTOMATION;
        int branchId = 0;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);
        testVehicle.setStaticVehicleData(null);

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, never()).getByVehicleIdV2(vehicleId);
        verify(fleetServiceProxy, never()).deleteVehicle(vehicleId);
    }

    @Test
    public void deleteSacVehicle_CanNotGetDynamicVehicleData() {
        VehicleType vehicleType = VehicleType.SAC_AUTOMATION;
        int branchId = 0;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);
        testVehicle.setDynamicVehicleData(null);

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, never()).getByVehicleIdV2(vehicleId);
        verify(fleetServiceProxy, never()).deleteVehicle(vehicleId);
    }

    @Test
    public void deleteSacVehicle_DeleteFromFleetFail() {
        VehicleType vehicleType = VehicleType.SAC_AUTOMATION;
        int branchId = 0;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);
        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);
        when(fleetServiceProxy.deleteVehicle(vehicleId))
                .thenReturn(false);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, times(1)).deleteVehicle(vehicleId);
        verify(fleetServiceProxy, never()).getByVehicleIdV2(vehicleId);
    }

    @Test
    public void deleteSacVehicle_NotDeletedFromFleet() {
        VehicleType vehicleType = VehicleType.SAC_AUTOMATION;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, 0, AcrissCode.CLMR,
                vehiclePoolId);

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);
        when(fleetServiceProxy.deleteVehicle(vehicleId))
                .thenReturn(true);
        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().
                setVin(vin).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().
                setVehicleId(vehicleId).
                setBasic(basic).
                setIsDeleted(false).build();
        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(vehicleV2);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, times(1)).deleteVehicle(vehicleId);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
    }

    @Test
    public void deleteRacVehicle_CanNotGetCurrentTestVehicleData() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(null);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, never()).getByVehicleIdV2(vehicleId);
        verify(fleetServiceProxy, never()).deleteVehicle(vehicleId);
        verify(racVehicleDeletionStepsProxy, never()).cancelVehicle(vehicleId);
    }

    @Test
    public void deleteRacVehicle_CanNotGetStaticVehicleData() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        int branchId = 0;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);
        testVehicle.setStaticVehicleData(null);

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, never()).getByVehicleIdV2(vehicleId);
        verify(fleetServiceProxy, never()).deleteVehicle(vehicleId);
        verify(racVehicleDeletionStepsProxy, never()).cancelVehicle(vehicleId);
    }

    @Test
    public void deleteRacVehicle_CanNotGetDynamicVehicleData() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        int branchId = 0;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);
        testVehicle.setDynamicVehicleData(null);

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, never()).getByVehicleIdV2(vehicleId);
        verify(fleetServiceProxy, never()).deleteVehicle(vehicleId);
        verify(racVehicleDeletionStepsProxy, never()).cancelVehicle(vehicleId);
    }

    @Test
    public void deleteRacVehicle_DeleteFromFleetFail() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        int branchId = 0;
        AcrissCode acrissCode = AcrissCode.CLMR;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, branchId, acrissCode,
                vehiclePoolId);
        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);
        when(fleetServiceProxy.deleteVehicle(vehicleId))
                .thenReturn(false);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, times(1)).deleteVehicle(vehicleId);
        verify(fleetServiceProxy, never()).getByVehicleIdV2(vehicleId);
        verify(racVehicleDeletionStepsProxy, never()).cancelVehicle(vehicleId);
    }

    @Test
    public void deleteRacVehicle_NotDeletedFromFleet() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, 0, AcrissCode.CLMR,
                vehiclePoolId);

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);
        when(fleetServiceProxy.deleteVehicle(vehicleId))
                .thenReturn(true);
        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().
                setVin(vin).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().
                setVehicleId(vehicleId).
                setBasic(basic).
                setIsDeleted(false).build();
        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(vehicleV2);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, times(1)).deleteVehicle(vehicleId);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
        verify(racVehicleDeletionStepsProxy, never()).cancelVehicle(vehicleId);
    }

    @Test
    public void deleteRacVehicle_NotDeletedFromClassic() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, vehicleType, 0, AcrissCode.CLMR,
                vehiclePoolId);

        setCleanupBehaviour(PublicJourneyState.PJS_ENDED, "AVAILABLE");

        when(testVehicleDataCollector.getCurrentTestVehicleData(vehicleId))
                .thenReturn(testVehicle);
        when(fleetServiceProxy.deleteVehicle(vehicleId))
                .thenReturn(true);
        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().
                setVin(vin).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().
                setVehicleId(vehicleId).
                setBasic(basic).
                setIsDeleted(true).build();
        when(fleetServiceProxy.getByVehicleIdV2(vehicleId))
                .thenReturn(vehicleV2);
        when(racVehicleDeletionStepsProxy.cancelVehicle(vehicleId))
                .thenReturn(false);

        boolean successDelete = vehicleCleaner.deleteVehicle(vehicleType, vehicleId);
        assertThat(successDelete).isFalse();

        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(vehicleId);
        verify(fleetServiceProxy, times(1)).deleteVehicle(vehicleId);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(vehicleId);
        verify(racVehicleDeletionStepsProxy, times(1)).cancelVehicle(vehicleId);
    }

    /**
     * Helper methods
     */

    private void setCleanupBehaviour(PublicJourneyState pjsEnded, String available) {
        // Set behaviour for vehicle cleanup before delete -> Cleanup not necessary
        GetVehicleJourneyResponse getVehicleJourneyResponse = GetVehicleJourneyResponse.newBuilder()
                .setJourneyId(journeyId)
                .setCurrentState(pjsEnded).build();
        when(journeyServiceProxy.getVehicleJourney(vehicleId))
                .thenReturn(getVehicleJourneyResponse);
        GetAvailabilityResponse getAvailabilityResponse = GetAvailabilityResponse.newBuilder()
                .setCurrentState(available).build();
        when(oneVehicleAvailabilityServiceProxy.getAvailability(vehicleId))
                .thenReturn(getAvailabilityResponse);
    }

}
