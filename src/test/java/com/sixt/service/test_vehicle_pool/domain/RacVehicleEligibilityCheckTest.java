package com.sixt.service.test_vehicle_pool.domain;

import com.sixt.service.branch.api.BranchAddress;
import com.sixt.service.branch.api.BranchOuterClass;
import com.sixt.service.test_vehicle_pool.infrastructure.BranchServiceProxy;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({RacVehicleEligibilityCheck.class})
public class RacVehicleEligibilityCheckTest {

    @Autowired
    private RacVehicleEligibilityCheck racVehicleEligibilityCheck;

    @MockBean
    private BranchServiceProxy branchServiceProxy;

    private static final int BRANCH_ID = 11;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLAR;

    @Test
    public void isRacVehicleEligible_True() {
        BranchOuterClass.BranchObject branchObject = getBranchObject("DE");
        when(branchServiceProxy.get(BRANCH_ID)).thenReturn(branchObject);

        boolean isRacVehicleEligible = racVehicleEligibilityCheck.isRacVehicleEligible(BRANCH_ID, ACRISS_CODE.name());

        assertThat(isRacVehicleEligible).isTrue();
    }

    @Test
    public void isRacVehicleEligible_False_CountryNotSupported() {
        BranchOuterClass.BranchObject branchObject = getBranchObject("XU");
        when(branchServiceProxy.get(BRANCH_ID)).thenReturn(branchObject);

        boolean isRacVehicleEligible = racVehicleEligibilityCheck.isRacVehicleEligible(BRANCH_ID, ACRISS_CODE.name());

        assertThat(isRacVehicleEligible).isFalse();
    }

    @Test
    public void isRacVehicleEligible_False_BranchCheckNotPossible() {
        when(branchServiceProxy.get(BRANCH_ID)).thenReturn(null);

        boolean isRacVehicleEligible = racVehicleEligibilityCheck.isRacVehicleEligible(BRANCH_ID, ACRISS_CODE.name());

        assertThat(isRacVehicleEligible).isFalse();
    }

    @Test
    public void isRacVehicleEligible_False_AcrissCodeNotSupportedForCountry() {
        BranchOuterClass.BranchObject branchObject = getBranchObject("DE");
        when(branchServiceProxy.get(BRANCH_ID)).thenReturn(branchObject);

        boolean isRacVehicleEligible = racVehicleEligibilityCheck.isRacVehicleEligible(BRANCH_ID, AcrissCode.XCAR.name());

        assertThat(isRacVehicleEligible).isFalse();
    }

    /**
     * Helper method
     */
    private BranchOuterClass.BranchObject getBranchObject(String countryCode) {
        BranchAddress.Country country = BranchAddress.Country.newBuilder()
                .setIso2Code(countryCode).build();
        BranchAddress.Address address = BranchAddress.Address.newBuilder()
                .setCountry(country).build();
        return BranchOuterClass.BranchObject.newBuilder()
                .setBranchId(BRANCH_ID)
                .addAddresses(address).build();
    }

}
