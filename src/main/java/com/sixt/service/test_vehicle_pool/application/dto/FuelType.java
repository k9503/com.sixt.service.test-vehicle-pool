package com.sixt.service.test_vehicle_pool.application.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum FuelType {
    EMPTY,
    DIESEL,
    GASOLINE,
    ELECTRIC,
    PREMIUM_UNLEADED,
    NONE;

    private static final Logger LOGGER = LoggerFactory.getLogger(FuelType.class);

    public static FuelType getEnum(String fuelTypeString) {
        for (FuelType value : FuelType.values()) {
            if (value.name().equals(fuelTypeString)) {
                return value;
            }
        }
        LOGGER.error("No vehicle fuel type enum value found for fuel type string: {}", fuelTypeString);
        return FuelType.EMPTY;
    }

}