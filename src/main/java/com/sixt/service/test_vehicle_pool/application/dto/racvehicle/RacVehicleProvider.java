package com.sixt.service.test_vehicle_pool.application.dto.racvehicle;


import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.testdatamanagement.rac.vehicle.creation.config.TestEnvironment;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.Company;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.Vehicle;
import com.sixt.testdatamanagement.rac.vehicle.creation.provider.AttributesDefList;
import com.sixt.testdatamanagement.rac.vehicle.creation.provider.VehicleProvider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class RacVehicleProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(RacVehicleProvider.class);

    public static Vehicle provideVehicle(TestEnvironment testEnvironment, Country country, String vin, String licensePlate,
                                         AcrissCode acrissCode, VehicleFuelType vehicleFuelType, boolean withWinterTyres,
                                         boolean fourWheels, boolean trailerCoupling) {

        Map<String, String> changeAttributesMap = new HashMap<>();
        String mappedFuelType = mapFuelType(vehicleFuelType);
        String mappedFuelCategory = mapFuelCategory(vehicleFuelType);
        String drivenWheels = mapDrivenWheels(fourWheels);

        switch (country) {
            case NL:
                changeAttributesMap.put(AttributesDefList.VIN.getAttribute(), vin);
                changeAttributesMap.put(AttributesDefList.LICENSE_PLATE.getAttribute(), licensePlate);
                changeAttributesMap.put(AttributesDefList.DRIVEN_WHEELS.getAttribute(), drivenWheels);
                changeAttributesMap.put(AttributesDefList.META_DRAW_BAR.getAttribute(), String.valueOf(trailerCoupling));

                return new VehicleProvider(testEnvironment, Company.NL, acrissCode, changeAttributesMap).provideVehicle();
            case DE:
                changeAttributesMap.put(AttributesDefList.VIN.getAttribute(), vin);
                changeAttributesMap.put(AttributesDefList.LICENSE_PLATE.getAttribute(), licensePlate);
                if (!StringUtils.isEmpty(mappedFuelType)) {
                    changeAttributesMap.put(AttributesDefList.FUEL_TYPE.getAttribute(), mappedFuelType);
                }
                if (!StringUtils.isEmpty(mappedFuelCategory)) {
                    changeAttributesMap.put(AttributesDefList.FUEL_CATEGORY.getAttribute(), mappedFuelCategory);
                }
                changeAttributesMap.put(AttributesDefList.WITH_WINTER_TYRES.getAttribute(), String.valueOf(withWinterTyres));
                changeAttributesMap.put(AttributesDefList.DRIVEN_WHEELS.getAttribute(), drivenWheels);
                changeAttributesMap.put(AttributesDefList.META_DRAW_BAR.getAttribute(), String.valueOf(trailerCoupling));

                return new VehicleProvider(testEnvironment, Company.DE, acrissCode, changeAttributesMap).provideVehicle();
            case IT:
                changeAttributesMap.put(AttributesDefList.VIN.getAttribute(), vin);
                changeAttributesMap.put(AttributesDefList.LICENSE_PLATE.getAttribute(), licensePlate);
                changeAttributesMap.put(AttributesDefList.DRIVEN_WHEELS.getAttribute(), drivenWheels);
                changeAttributesMap.put(AttributesDefList.META_DRAW_BAR.getAttribute(), String.valueOf(trailerCoupling));

                return new VehicleProvider(testEnvironment, Company.IT, acrissCode, changeAttributesMap).provideVehicle();
            case US:
                changeAttributesMap.put(AttributesDefList.VIN.getAttribute(), vin);
                changeAttributesMap.put(AttributesDefList.LICENSE_PLATE.getAttribute(), licensePlate);
                if (!StringUtils.isEmpty(mappedFuelType)) {
                    changeAttributesMap.put(AttributesDefList.FUEL_TYPE.getAttribute(), mappedFuelType);
                }
                if (!StringUtils.isEmpty(mappedFuelCategory)) {
                    changeAttributesMap.put(AttributesDefList.FUEL_CATEGORY.getAttribute(), mappedFuelCategory);
                }
                changeAttributesMap.put(AttributesDefList.WITH_WINTER_TYRES.getAttribute(), String.valueOf(withWinterTyres));
                changeAttributesMap.put(AttributesDefList.DRIVEN_WHEELS.getAttribute(), drivenWheels);
                changeAttributesMap.put(AttributesDefList.META_DRAW_BAR.getAttribute(), String.valueOf(trailerCoupling));

                return new VehicleProvider(testEnvironment, Company.USA, acrissCode, changeAttributesMap).provideVehicle();
            case ES:
                changeAttributesMap.put(AttributesDefList.VIN.getAttribute(), vin);
                changeAttributesMap.put(AttributesDefList.LICENSE_PLATE.getAttribute(), licensePlate);
                changeAttributesMap.put(AttributesDefList.DRIVEN_WHEELS.getAttribute(), drivenWheels);
                changeAttributesMap.put(AttributesDefList.META_DRAW_BAR.getAttribute(), String.valueOf(trailerCoupling));

                return new VehicleProvider(testEnvironment, Company.ES, acrissCode, changeAttributesMap).provideVehicle();
            case FR:
                changeAttributesMap.put(AttributesDefList.VIN.getAttribute(), vin);
                changeAttributesMap.put(AttributesDefList.LICENSE_PLATE.getAttribute(), licensePlate);
                changeAttributesMap.put(AttributesDefList.DRIVEN_WHEELS.getAttribute(), drivenWheels);
                changeAttributesMap.put(AttributesDefList.META_DRAW_BAR.getAttribute(), String.valueOf(trailerCoupling));

                return new VehicleProvider(testEnvironment, Company.FR, acrissCode, changeAttributesMap).provideVehicle();
            case UK:
            case GB:
                changeAttributesMap.put(AttributesDefList.VIN.getAttribute(), vin);
                changeAttributesMap.put(AttributesDefList.LICENSE_PLATE.getAttribute(), licensePlate);
                changeAttributesMap.put(AttributesDefList.DRIVEN_WHEELS.getAttribute(), drivenWheels);

                return new VehicleProvider(testEnvironment, Company.UK, acrissCode, changeAttributesMap).provideVehicle();
            case AT:
                changeAttributesMap.put(AttributesDefList.VIN.getAttribute(), vin);
                changeAttributesMap.put(AttributesDefList.LICENSE_PLATE.getAttribute(), licensePlate);
                changeAttributesMap.put(AttributesDefList.DRIVEN_WHEELS.getAttribute(), drivenWheels);
                return new VehicleProvider(testEnvironment, Company.AUT, acrissCode, changeAttributesMap).provideVehicle();
            default:
                LOGGER.error("Country {} is not supported", country.name());
                return null;
        }
    }

    private static String mapFuelType(VehicleFuelType vehicleFuelType) {
        String mappedFuelType;

        switch (vehicleFuelType) {
            case GASOLINE:
                mappedFuelType = "U";
                break;
            case DIESEL:
                mappedFuelType = "D";
                break;
            // No special mapping for ELECTRIC / HYBRID - Take default from template
            case ELECTRIC:
            case HYBRID:
                mappedFuelType = "";
                break;
            default:
                LOGGER.error("No matching internal fuel type found for <{}>", vehicleFuelType.name());
                mappedFuelType = "";
        }

        return mappedFuelType;
    }

    private static String mapFuelCategory(VehicleFuelType vehicleFuelType) {
        String mappedFuelCategory;

        switch (vehicleFuelType) {
            case GASOLINE:
                mappedFuelCategory = "G";
                break;
            case DIESEL:
                mappedFuelCategory = "D";
                break;
            // No special mapping for ELECTRIC / HYBRID - Take default from template
            case ELECTRIC:
            case HYBRID:
                mappedFuelCategory = "";
                break;
            default:
                LOGGER.error("No matching fuel category found for fuel type <{}>", vehicleFuelType.name());
                mappedFuelCategory = "";
        }

        return mappedFuelCategory;
    }

    private static String mapDrivenWheels(boolean fourWheels) {
        String mappedDrivenWheels;

        if (fourWheels) {
            mappedDrivenWheels = "4";
        } else {
            mappedDrivenWheels = "F";
        }

        return mappedDrivenWheels;
    }

}
