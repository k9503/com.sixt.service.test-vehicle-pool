package com.sixt.service.test_vehicle_pool.application.dto;


public enum VehicleLockState {
    LOCKED,
    UNLOCKED,
    LOCK_FAILED,
    UNLOCK_FAILED,
    LOCK_UNKNOWN,
    UNKNOWN
}
