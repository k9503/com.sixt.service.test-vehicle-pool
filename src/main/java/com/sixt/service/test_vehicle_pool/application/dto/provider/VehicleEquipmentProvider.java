package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.test_vehicle_pool.application.dto.FuelType;
import org.apache.commons.lang3.RandomStringUtils;
import com.sixt.service.fleet.api.VehicleV2.Equipment;

public class VehicleEquipmentProvider {

    public static Equipment createEquipment(boolean keyless, FuelType fuelType1) {
        final Equipment.Builder equipmentBuilder = Equipment.newBuilder();
        if (fuelType1 == FuelType.ELECTRIC) {
            equipmentBuilder.setChargeCable(true);
        }

        return equipmentBuilder
                .setDigiTacho(false)
                .setDrawBar(false)
                .setGreenEmissionSticker(true)
                .setNavigation(true)
                .setNavigationType("OEM")
                .setRemoteControlParkingHeater(false)
                .setSpareWheel(false)
                .setSpareWheelType("None")
                .setSummerTiresSticker(true)
                .setTrunkCover(true)
                .setParcelShelf(true)
                .setVignette(true)
                .setVignetteType("Yearly")
                .setVignetteNumber(RandomStringUtils.randomAlphabetic(20))
                .setWinterSuitableTires(true)
                .setWinterSuitableTiresType("Pirelli Winter")
                .setDriversCab(false)
                .setLiftingRamp(false)
                .setBluetooth(true)
                .setBarcode(false)
                .setAdBlue(true)
                .setPollutionBadge(false)
                .setListPriceIncludingEquipment("60000")
                .setCustomsCleared(true)
                .setLasiSystem(false)
                .setLasiSystemType("None")
                .setIsKeyless(keyless)
                .build();
    }

}
