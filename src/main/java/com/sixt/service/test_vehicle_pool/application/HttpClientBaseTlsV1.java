package com.sixt.service.test_vehicle_pool.application;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Component
public class HttpClientBaseTlsV1 extends HttpClientBase {

    public HttpClientBaseTlsV1() {
        super(new OkHttpClient().newBuilder().connectionSpecs(Arrays.asList(ConnectionSpec.COMPATIBLE_TLS))
                .readTimeout(180, TimeUnit.SECONDS)
                .build());
    }

}