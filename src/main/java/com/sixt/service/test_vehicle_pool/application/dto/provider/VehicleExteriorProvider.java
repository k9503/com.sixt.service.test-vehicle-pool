package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.fleet.api.VehicleV2.Exterior;

public class VehicleExteriorProvider {
    private final static String DEFAULT_PAINTING = "M";
    private final static String DEFAULT_PAINTING_COLOR_DESCR = "ColorDescription";
    private final static String DEFAULT_PAINTING_COLOR2 = "Green";
    private final static String DEFAULT_PAINTING_COLOR_TYPE = "Metallic";

    public static Exterior createExterior(String color, String imageUrl) {
        return Exterior.newBuilder()
                .setPaintColorBase(color)
                .setImageUrl(imageUrl)
                .setPaint(DEFAULT_PAINTING)
                .setPaintColorDescription(DEFAULT_PAINTING_COLOR_DESCR)
                .setPaintColor2(DEFAULT_PAINTING_COLOR2)
                .setExternalColorType(DEFAULT_PAINTING_COLOR_TYPE)
                .build();
    }

}