package com.sixt.service.test_vehicle_pool.application;

import com.google.gson.Gson;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.RequestBody;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class IdentityApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(IdentityApi.class);
    private HttpClientBase client;
    private Gson gson = new Gson();

    @Value("${identity.host}")
    private String host;

    @Autowired
    HttpClientBase httpClientBase;

    @Autowired
    public IdentityApi(
            HttpClientBase httpClientBase) {
        this.client = httpClientBase;
    }

    public String getToken(String realm, String clientId, String clientSecret) {
        final String PATH = "/auth/realms/" + realm + "/protocol/openid-connect/token";
        String url = host + PATH;

        RequestBody formBody = new FormBody.Builder()
                .add("client_id", clientId)
                .add("grant_type", "client_credentials")
                .add("client_secret", clientSecret)
                .build();

        try {
            Headers headers = new Builder().build();
            String response = client.post(url, formBody, headers);
            GetTokenResponse getTokenResponse = gson.fromJson(response, GetTokenResponse.class);
            if (StringUtils.isNotBlank(getTokenResponse.getAccessToken())) {
                LOGGER.debug("Successfully received token from identity API");
                return getTokenResponse.getAccessToken();
            }
        } catch (Exception e) {
            LOGGER.error("Failed to create token: {}", e.getMessage());
        }

        return null;
    }

    private class GetTokenResponse {
        private String access_token;

        private String getAccessToken() {
            return access_token;
        }
    }

}
