package com.sixt.service.test_vehicle_pool.application.scheduler;

import com.sixt.service.test_vehicle_pool.application.ScheduledTasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ManualRacVehicleDeletionScheduler {

    @Autowired
    private ScheduledTasks scheduledTasks;

    /**
     * Scheduler to delete FASTLANE_MANUAL and RAC_TELEMATIC_MANUAL vehicles 7 days after creation
     * to avoid overloading branches / stations with test vehicles
     * <p>
     * Run this scheduler every hour / 3600 sec
     */
    @Scheduled(fixedRate = 3600000)
    public void manualRacVehicleDeletionScheduler() {
        scheduledTasks.manualRacVehicleDeletion();
    }

}