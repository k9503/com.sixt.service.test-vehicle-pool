package com.sixt.service.test_vehicle_pool.application.dto;

import com.sixt.service.test_vehicle_pool.api.TestLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class RandomVehiclePositionProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(RandomVehiclePositionProvider.class);

    public static VehiclePosition createPosition(TestLocation testLocation) {
        float locationLat;
        float locationLong;
        int radiusMeter = 200;

        switch (testLocation) {
            case MUNICH:
                locationLat = 48.042964F;
                locationLong = 11.510944F;
                break;
            case BANGALORE:
                locationLat = 12.9325876F;
                locationLong = 77.6942333F;
                break;
            case KIEV:
                locationLat = 50.47F;
                locationLong = 30.48F;
                break;
            case ROTTERDAM:
                locationLat = 51.928375F;
                locationLong = 4.4816933F;
                break;
            case AMSTERDAM:
                locationLat = 52.3545362F;
                locationLong = 4.7638781F;
                break;
            case HAMBURG:
                locationLat = 53.552075F;
                locationLong = 9.996762F;
                break;
            default:
                locationLat = 11.11F;
                locationLong = 22.22F;
                LOGGER.debug("Using default position {},{} as no matching location found", locationLat, locationLong);
                return new VehiclePosition(locationLat, locationLong);
        }

        VehiclePosition randomPosition = getRandomVehiclePosition(locationLat, locationLong, radiusMeter);
        LOGGER.debug("Created random position {},{} for test location {}", randomPosition.getLatitude(), randomPosition.getLongitude(),
                testLocation);

        return randomPosition;
    }

    public static VehiclePosition getRandomVehiclePosition(float lat, float lon, int radiusMeter) {
        Random random = new Random();

        // Convert radius from meters to degrees
        double radiusInDegrees = radiusMeter / 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(Math.toRadians(lon));

        double foundLatitude = new_x + lat;
        double foundLongitude = y + lon;

        LOGGER.debug("Calculated random position {},{} ", foundLatitude, foundLongitude);

        return new VehiclePosition((float) foundLatitude, (float) foundLongitude);
    }

}
