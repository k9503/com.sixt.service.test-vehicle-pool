package com.sixt.service.test_vehicle_pool.application.dto.racvehicle;

import com.sixt.service.branch.api.BranchOuterClass.*;

public class BasicRacVehicle {

    private String vehicleId;
    private int company;
    private BranchObject branchObject;
    private String licensePlate;

    public BasicRacVehicle(String vehicleId, int company, BranchObject branchObject, String licensePlate) {
        this.vehicleId = vehicleId;
        this.company = company;
        this.branchObject = branchObject;
        this.licensePlate = licensePlate;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }

    public BranchObject getBranchObject() {
        return branchObject;
    }

    public void setBranchObject(BranchObject branchObject) {
        this.branchObject = branchObject;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }
}
