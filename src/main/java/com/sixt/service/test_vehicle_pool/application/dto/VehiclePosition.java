package com.sixt.service.test_vehicle_pool.application.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class VehiclePosition {

    private float longitude;
    private float latitude;

    public VehiclePosition(Position position) {
        this.longitude = position.getLongitude();
        this.latitude = position.getLatitude();
    }

    public VehiclePosition(@JsonProperty("latitude") float latitude, @JsonProperty("longitude") float longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        VehiclePosition that = (VehiclePosition) o;

        // compare latitude and longitude with precision
        if ((Math.abs(that.getLatitude() - getLatitude()) < 0.001) &&
                (Math.abs(that.getLongitude() - getLongitude()) < 0.001)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getLongitude())
                .append(getLatitude())
                .toHashCode();
    }

    @Override
    public String toString() {
        return "VehiclePosition{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }

}
