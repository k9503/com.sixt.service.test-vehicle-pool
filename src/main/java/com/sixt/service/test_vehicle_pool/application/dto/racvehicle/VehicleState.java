package com.sixt.service.test_vehicle_pool.application.dto.racvehicle;

public enum VehicleState {
    READY_FOR_RENTAL(1),
    AVAILABLE(4),
    FASTLANE(22);

    private final int value;

    VehicleState(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }
}
