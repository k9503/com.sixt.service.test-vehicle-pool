package com.sixt.service.test_vehicle_pool.application.dto;

public final class VehicleAddressBuilder {
    private VehicleAddress vehicleAddress;

    private VehicleAddressBuilder() {
        this.vehicleAddress = new VehicleAddress();
    }

    public static VehicleAddressBuilder aVehicleAddress() {
        return new VehicleAddressBuilder();
    }

    public VehicleAddressBuilder withStreet(String street) {
        this.vehicleAddress.setStreet(street);
        return this;
    }

    public VehicleAddressBuilder withHouseNumber(String houseNumber) {
        this.vehicleAddress.setHouseNumber(houseNumber);
        return this;
    }

    public VehicleAddressBuilder withPostCode(String postCode) {
        this.vehicleAddress.setPostcode(postCode);
        return this;
    }

    public VehicleAddressBuilder withCity(String city) {
        this.vehicleAddress.setCity(city);
        return this;
    }

    public VehicleAddressBuilder withCountryCode(String countryCode) {
        this.vehicleAddress.setCountryCode(countryCode);
        return this;
    }

    public VehicleAddress build() {
        return this.vehicleAddress;
    }
}
