package com.sixt.service.test_vehicle_pool.application;

import okhttp3.*;
import okio.Buffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class HttpClientBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpClientBase.class);
    private OkHttpClient httpClient;

    @Autowired
    public HttpClientBase(
            OkHttpClient okHttpClient) {
        this.httpClient = okHttpClient;
    }

    public String get(String url, Headers headers) throws Exception {
        Request request = new Request.Builder()
                .url(url)
                .headers(headers)
                .build();
        LOGGER.info("GET Request: {}", request.toString());

        try (Response response = httpClient.newCall(request).execute()) {
            LOGGER.info("GET response code: <{}> {}", response.code(), response.message());
            if (!response.isSuccessful()) {
                LOGGER.error("Got error code for HTTP GET request. Got response {} with body {}", response.toString(), response.body().string());
                throw new HttpRequestFailedException("No successful response for HTTP GET request");
            }

            ResponseBody responseBody = response.body();
            if (responseBody != null) {
                return responseBody.string();
            } else {
                LOGGER.error("Empty response body in GET response");
                return null;
            }
        }
    }

    public String getQueryParams(String url, Map<String, String> params, Headers headers) throws Exception {
        HttpUrl.Builder httpUrlBuilder = HttpUrl.parse(url).newBuilder();
        if (!params.isEmpty()) {
            for (Map.Entry<String, String> param : params.entrySet()) {
                httpUrlBuilder.addQueryParameter(param.getKey(), param.getValue());
            }
        }

        Request request = new Request.Builder()
                .url(httpUrlBuilder.build())
                .headers(headers)
                .build();
        LOGGER.info("GET Request: {}", request.toString());

        try (Response response = httpClient.newCall(request).execute()) {
            LOGGER.info("GET response code: <{}> {}", response.code(), response.message());
            if (!response.isSuccessful()) {
                LOGGER.error("Got error code for HTTP GET request. Got response {} with body {}", response.toString(), response.body().string());
                throw new HttpRequestFailedException("No successful response for HTTP GET request");
            }

            ResponseBody responseBody = response.body();
            if (responseBody != null) {
                return responseBody.string();
            } else {
                LOGGER.error("Empty response body in GET response");
                return null;
            }
        }
    }

    public String post(String url, String json, Headers headers, MediaType type) throws Exception {
        RequestBody body = RequestBody.create(json, type);

        return post(url, body, headers);
    }

    public String post(String url, RequestBody body, Headers headers) throws Exception {
        Request request = new Request.Builder()
                .url(url)
                .headers(headers)
                .post(body)
                .build();
        LOGGER.info("POST request: {}", request.toString());
        LOGGER.debug("POST body: {}", getRequestBodyAsString(request));

        try (Response response = httpClient.newCall(request).execute()) {
            LOGGER.info("POST response code: <{}> {}", response.code(), response.message());

            if (!response.isSuccessful()) {
                LOGGER.error("Got error code for HTTP POST request. Got response {} with body {}", response.toString(), response.body().string());
                throw new HttpRequestFailedException("No successful response for HTTP POST request");
            }

            ResponseBody responseBody = response.body();
            if (responseBody != null) {
                return responseBody.string();
            } else {
                LOGGER.error("Empty response body in POST response");
                return null;
            }
        }
    }

    public String put(String url, String json, Headers headers, MediaType type) throws Exception {
        RequestBody body = RequestBody.create(json, type);

        Request request = new Request.Builder()
                .url(url)
                .headers(headers)
                .put(body)
                .build();
        LOGGER.info("PUT request: {}", request.toString());
        LOGGER.debug("PUT body: {}", getRequestBodyAsString(request));

        try (Response response = httpClient.newCall(request).execute()) {
            LOGGER.info("PUT response code: <{}> {}", response.code(), response.message());
            if (!response.isSuccessful()) {
                LOGGER.error("Got error code for HTTP PUT request. Got response {} with body {}", response.toString(), response.body().string());
                throw new HttpRequestFailedException("No successful response for HTTP PUT request");
            }

            ResponseBody responseBody = response.body();
            if (responseBody != null) {
                return responseBody.string();
            } else {
                LOGGER.error("Empty response body in PUT response");
                return null;
            }
        }
    }

    private String getRequestBodyAsString(Request request) {
        if (request.body() != null) {
            try {
                final Request copy = request.newBuilder().build();
                final Buffer buffer = new Buffer();
                copy.body().writeTo(buffer);
                return buffer.readUtf8();
            } catch (final IOException e) {
                LOGGER.error("Failed to create string from request body: " + e.getMessage());
            }
        }

        return "";
    }
}
