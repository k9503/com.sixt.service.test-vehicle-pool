package com.sixt.service.test_vehicle_pool.application.scheduler;

import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.ScheduledTasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SacVehicleCreationScheduler {

    @Autowired
    private ScheduledTasks scheduledTasks;

    /**
     * Scheduler to check if enough test vehicles of type SAC_AUTOMATION are available for
     * the automated tests.
     * If not enough vehicles of a type are available then create new ones to re-fill the pool
     * <p>
     * Run this scheduler every minute / 60 sec
     */
    @Scheduled(fixedRate = 60000)
    public void sacVehicleCreationScheduler() {
        scheduledTasks.createVehiclesInPool(VehicleType.SAC_AUTOMATION);
    }

}
