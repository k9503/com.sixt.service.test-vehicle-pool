package com.sixt.service.test_vehicle_pool.application.dto;

public enum FleetName {
    ONE("ONE"),
    RAC("RAC");

    private String fleetName;

    FleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getFleetName() {
        return fleetName;
    }

    @Override
    public String toString() {
        return "FleetName{" +
                "fleetName='" + fleetName + '\'' +
                '}';
    }

}