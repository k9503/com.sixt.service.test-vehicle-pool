package com.sixt.service.test_vehicle_pool.application.dto;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public enum Color {
    EMPTY("", ""),
    PINK("PNK", "pink"),
    BLACK("BLK", "black");

    private static final Logger LOGGER = LoggerFactory.getLogger(Color.class);

    public static final String FLEET_PREFIX = "Color_";

    private String colorCode;
    private String colorEnglishDefault;

    Color(String colorCode, String colorEnglishDefault) {
        this.colorCode = colorCode;
        this.colorEnglishDefault = colorEnglishDefault;
    }

    public String getColorCode() {
        return this.colorCode;
    }

    public String getColorEnglishDefault() {
        return this.colorEnglishDefault;
    }

    public static Color getEnum(String code){
        String colorCode = code.replace(Color.FLEET_PREFIX, "");
        for (Color value: Color.values()) {
            if(value.getColorCode().equals(colorCode)){
                return value;
            }
        }
        LOGGER.error("No vehicle color found for code: {}", code);
        return Color.EMPTY;
    }

    public static String getColorCode(String color){
        for (Color value: Color.values()) {
            if(value.getColorEnglishDefault().equals(color)){
                return value.getColorCode();
            }
        }
        LOGGER.error("No vehicle color code found for color: {}", color);
        return StringUtils.EMPTY;
    }

    public static Color random() {
        Color[] allColors = Color.values();

        // Skip empty color
        List<Color> colors = new ArrayList<>();
        for (Color currentColor : allColors) {
            if (currentColor != EMPTY) {
                colors.add(currentColor);
            }
        }

        Random random = new Random();

        int index = random.nextInt(colors.size());
        return colors.get(index);
    }

}