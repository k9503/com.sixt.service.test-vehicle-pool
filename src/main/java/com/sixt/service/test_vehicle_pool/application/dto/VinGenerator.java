package com.sixt.service.test_vehicle_pool.application.dto;

import com.sixt.service.test_vehicle_pool.api.VehicleType;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum VinGenerator {
    EMPTY("", VehicleType.UNRECOGNIZED),
    SAC_AUTOMATION("FAKESCA", VehicleType.SAC_AUTOMATION),
    SAC_PERFORMANCE("FAKESCP", VehicleType.SAC_PERFORMANCE),
    SAC_MANUAL("FAKESCM", VehicleType.SAC_MANUAL),
    SAC_SIMULATOR("VESMSAC", VehicleType.SAC_SIMULATOR),
    FASTLANE_AUTOMATION("FAKEFLA", VehicleType.FASTLANE_AUTOMATION),
    FASTLANE_MANUAL("FAKEFLM", VehicleType.FASTLANE_MANUAL),
    RAC_TELEMATIC_AUTOMATION("FAKERTA", VehicleType.RAC_TELEMATIC_AUTOMATION),
    RAC_TELEMATIC_MANUAL("FAKERTM", VehicleType.RAC_TELEMATIC_MANUAL),
    RAC_AUTOMATION("FAKERCA", VehicleType.RAC_AUTOMATION);

    private static final Logger LOGGER = LoggerFactory.getLogger(VinGenerator.class);

    private String prefix;
    private VehicleType vehicleType;

    VinGenerator(String prefix, VehicleType vehicleType) {
        this.prefix = prefix;
        this.vehicleType = vehicleType;
    }

    private VehicleType getVehicleType() {
        return this.vehicleType;
    }

    private String getPrefix() {
        return this.prefix;
    }

    public static String getVinByType(VehicleType vehicleType) {
        for (VinGenerator vinGenerator : VinGenerator.values()) {
            if (vinGenerator.getVehicleType().equals(vehicleType)) {
                String vin = vinGenerator.getPrefix() + RandomStringUtils.randomAlphanumeric(10).toUpperCase();
                // Characters I, O and Q are not allowed in a VIN and have to be replaced
                vin = vin.replaceAll("[IOQ]", "R");
                return vin;
            }
        }
        LOGGER.error("No VIN prefix found for vehicle type: {}", vehicleType);
        return null;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("prefix", prefix)
                .append("vehicleType", vehicleType)
                .toString();
    }

}