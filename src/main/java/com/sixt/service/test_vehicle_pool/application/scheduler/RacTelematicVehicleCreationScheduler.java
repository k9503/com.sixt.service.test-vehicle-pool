package com.sixt.service.test_vehicle_pool.application.scheduler;

import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.ScheduledTasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RacTelematicVehicleCreationScheduler {

    @Autowired
    private ScheduledTasks scheduledTasks;

    /**
     * Scheduler to check if enough test vehicles of type RAC_TELEMATIC_AUTOMATION are available for
     * the automated tests.
     * If not enough vehicles of a type are available then create new ones to re-fill the pool
     * <p>
     * Run this scheduler every minute / 60 sec
     */
    @Scheduled(fixedRate = 60000)
    public void racTelematicVehicleCreationScheduler() {
        scheduledTasks.createVehiclesInPool(VehicleType.RAC_TELEMATIC_AUTOMATION);
    }

}
