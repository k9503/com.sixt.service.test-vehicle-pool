package com.sixt.service.test_vehicle_pool.application;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.event.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.MediaType;

import static net.logstash.logback.marker.Markers.append;

@Component
public class EventApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventApi.class);
    private final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private HttpClientBase client;

    @Value("${api.host}")
    private String host;
    @Value("${app.api.clientid}")
    private String clientId;
    @Value("${app.api.clientsecret}")
    private String clientSecret;
    @Value("${environment.env}")
    private String environment;

    @Autowired
    HttpClientBase httpClientBase;

    @Autowired
    IdentityApi identityApi;

    @Autowired
    public EventApi(
            HttpClientBase httpClientBase) {
        this.client = httpClientBase;
    }

    public void publishFleetVehicleCreatedEvent(VehicleV2 fleetVehicle) {
        final String PATH = "/v1/event/publish";
        String url = host + PATH;

        FleetVehicleCreatedEvent fleetVehicleCreatedEvent = new FleetVehicleCreatedEvent(fleetVehicle);

        try {
            Headers headers = new Builder().build();

            if (environment.equals("stage")) {
                String token = identityApi.getToken("One", clientId, clientSecret);
                headers = new Builder()
                        .set("Authorization", "Bearer " + token)
                        .build();
            }

            client.post(url, fleetVehicleCreatedEvent.getAsJson().toString(), headers, JSON);
            LOGGER.info(append("vehicleId", fleetVehicle.getVehicleId()), "Published FleetVehicleCreated event");
        } catch (Exception exception) {
            LOGGER.error(append("vehicleId", fleetVehicle.getVehicleId()), "Failed to publish FleetVehicleCreated event", exception);
        }
    }

    public void publishFleetOpsUsageStartedEvent(TestVehicle testVehicle) {
        final String PATH = "/v1/event/publish";
        String url = host + PATH;

        FleetOpsUsageStartedEvent fleetOpsUsageStartedEvent = new FleetOpsUsageStartedEvent(testVehicle);

        try {
            Headers headers = new Builder().build();

            if (environment.equals("stage")) {
                String token = identityApi.getToken("One", clientId, clientSecret);
                headers = new Builder()
                        .set("Authorization", "Bearer " + token)
                        .build();
            }

            client.post(url, fleetOpsUsageStartedEvent.getAsJson().toString(), headers, JSON);
            LOGGER.info(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Published FleetOpsUsageStarted event");
        } catch (Exception exception) {
            LOGGER.error(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Failed to publish FleetOpsUsageStarted event", exception);
        }
    }

    public void publishFleetOpsUsageEndedEvent(TestVehicle testVehicle) {
        final String PATH = "/v1/event/publish";
        String url = host + PATH;

        FleetOpsUsageEndedEvent fleetOpsUsageEndedEvent = new FleetOpsUsageEndedEvent(testVehicle);

        try {
            Headers headers = new Builder().build();

            if (environment.equals("stage")) {
                String token = identityApi.getToken("One", clientId, clientSecret);
                headers = new Builder()
                        .set("Authorization", "Bearer " + token)
                        .build();
            }

            client.post(url, fleetOpsUsageEndedEvent.getAsJson().toString(), headers, JSON);
            LOGGER.info(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Published FleetOpsUsageEnded event");
        } catch (Exception exception) {
            LOGGER.error(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Failed to publish FleetOpsUsageEnded event", exception);
        }
    }

    public void publishFleetAssignmentStartedEvent(TestVehicle testVehicle, String fleetAssignmentId) {
        final String PATH = "/v1/event/publish";
        String url = host + PATH;

        FleetAssignmentStartedEvent fleetAssignmentStartedEvent = new FleetAssignmentStartedEvent(testVehicle, fleetAssignmentId);

        try {
            Headers headers = new Builder().build();

            if (environment.equals("stage")) {
                String token = identityApi.getToken("One", clientId, clientSecret);
                headers = new Builder()
                        .set("Authorization", "Bearer " + token)
                        .build();
            }

            client.post(url, fleetAssignmentStartedEvent.getAsJson().toString(), headers, JSON);
            LOGGER.info(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Published FleetAssignmentStarted event");
        } catch (Exception exception) {
            LOGGER.error(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Failed to publish FleetAssignmentStarted event", exception);
        }
    }

    public void publishFleetAssignmentStartedDefleetEvent(TestVehicle testVehicle, String fleetAssignmentId) {
        final String PATH = "/v1/event/publish";
        String url = host + PATH;

        FleetAssignmentStartedDefleetEvent fleetAssignmentStartedDefleetEvent = new FleetAssignmentStartedDefleetEvent(testVehicle, fleetAssignmentId);

        try {
            Headers headers = new Builder().build();

            if (environment.equals("stage")) {
                String token = identityApi.getToken("One", clientId, clientSecret);
                headers = new Builder()
                        .set("Authorization", "Bearer " + token)
                        .build();
            }

            client.post(url, fleetAssignmentStartedDefleetEvent.getAsJson().toString(), headers, JSON);
            LOGGER.info(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Published FleetAssignmentStarted event for defleet");
        } catch (Exception exception) {
            LOGGER.error(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Failed to publish FleetAssignmentStarted event for defleet", exception);
        }
    }

    public void publishFleetAssignmentEndedEvent(TestVehicle testVehicle, String fleetAssignmentId) {
        final String PATH = "/v1/event/publish";
        String url = host + PATH;

        FleetAssignmentEndedEvent fleetAssignmentEndedEvent = new FleetAssignmentEndedEvent(testVehicle, fleetAssignmentId);

        try {
            Headers headers = new Builder().build();

            if (environment.equals("stage")) {
                String token = identityApi.getToken("One", clientId, clientSecret);
                headers = new Builder()
                        .set("Authorization", "Bearer " + token)
                        .build();
            }

            client.post(url, fleetAssignmentEndedEvent.getAsJson().toString(), headers, JSON);
            LOGGER.info(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Published FleetAssignmentEnded event");
        } catch (Exception exception) {
            LOGGER.error(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()), "Failed to publish FleetAssignmentEnded event", exception);
        }
    }

}
