package com.sixt.service.test_vehicle_pool.application.dto;


import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class VehiclePrice {

    private int net; // price in cents
    private int gross;
    private String currency; // currency code
    private String unit;

    public VehiclePrice() {
        this.net = 6;
        this.gross = 6;
        this.currency = "EUR";
        this.unit = "min";
    }

    public VehiclePrice(int net, int gross) {
        this.net = net;
        this.gross = gross;
        this.currency = "EUR";
        this.unit = "min";
    }

    public int getNet() {
        return net;
    }

    public void setNet(int net) {
        this.net = net;
    }

    public int getGross() {
        return gross;
    }

    public void setGross(int gross) {
        this.gross = gross;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        VehiclePrice that = (VehiclePrice) o;

        return new EqualsBuilder()
                .append(net, that.net)
                .append(gross, that.gross)
                .append(currency, that.currency)
                .append(unit, that.unit)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(net)
                .append(gross)
                .append(currency)
                .append(unit)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("net", net)
                .append("gross", gross)
                .append("currency", currency)
                .append("unit", unit)
                .toString();
    }

}
