package com.sixt.service.test_vehicle_pool.application.dto.event;

import com.google.gson.JsonObject;
import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.application.dto.*;
import org.apache.commons.lang3.StringUtils;

import java.time.Instant;

public class FleetVehicleCreatedEvent {

    private VehicleV2 staticVehicleData;

    public FleetVehicleCreatedEvent(VehicleV2 staticVehicleData) {
        this.staticVehicleData = staticVehicleData;
    }

    public JsonObject getAsJson() {
        Instant eventTimestamp = Instant.now();

        // Create meta
        JsonObject meta = new JsonObject();
        meta.addProperty("name", "FleetVehicleCreated");
        meta.addProperty("timestamp", eventTimestamp.toString());
        meta.addProperty("distribution_key", staticVehicleData.getVehicleId());

        // Create vehicle core data
        JsonObject vehicleCore = new JsonObject();
        vehicleCore.addProperty("make", staticVehicleData.getBasic().getMake());
        vehicleCore.addProperty("model", staticVehicleData.getBasic().getModel());
        vehicleCore.addProperty("model_annex", staticVehicleData.getBasic().getModelAnnex());
        vehicleCore.addProperty("jato_vehicle_id", staticVehicleData.getBasic().getJatoVehicleId());
        vehicleCore.addProperty("jato_version_name", staticVehicleData.getBasic().getJatoVersionName());
        vehicleCore.addProperty("sixt_model", staticVehicleData.getBasic().getSixtModel());
        vehicleCore.addProperty("sixt_type", staticVehicleData.getBasic().getSixtType());
        vehicleCore.addProperty("vehicle_type", staticVehicleData.getBasic().getVehicleType());
        vehicleCore.addProperty("platform_type", staticVehicleData.getBasic().getPlatformType());
        vehicleCore.addProperty("body_type", staticVehicleData.getBasic().getBodyType());
        vehicleCore.addProperty("kw", staticVehicleData.getBasic().getKw());
        vehicleCore.addProperty("ps", staticVehicleData.getBasic().getPs());
        vehicleCore.addProperty("ccm", staticVehicleData.getBasic().getCcm());
        vehicleCore.addProperty("vin", staticVehicleData.getBasic().getVin());
        vehicleCore.addProperty("license_plate", staticVehicleData.getBasic().getLicensePlate());
        vehicleCore.addProperty("creation_date", staticVehicleData.getBasic().getCreationDate());
        vehicleCore.addProperty("company_id", staticVehicleData.getBasic().getCompanyId());
        vehicleCore.addProperty("use_status", staticVehicleData.getBasic().getUseStatus());
        vehicleCore.addProperty("transmission_type", staticVehicleData.getBasic().getTransmissionType());
        vehicleCore.addProperty("driven_wheels", staticVehicleData.getBasic().getDrivenWheels());
        vehicleCore.addProperty("fuel_type", staticVehicleData.getBasic().getFuelType());
        vehicleCore.addProperty("fuel_type2", staticVehicleData.getBasic().getFuelType2());
        vehicleCore.addProperty("seating_capacity", String.valueOf(staticVehicleData.getBasic().getSeatingCapacity()));
        vehicleCore.addProperty("number_of_doors", String.valueOf(staticVehicleData.getBasic().getNumberOfDoors()));
        vehicleCore.addProperty("co2", staticVehicleData.getBasic().getCo2());
        vehicleCore.addProperty("co2_unit", staticVehicleData.getBasic().getCo2Unit());
        vehicleCore.addProperty("internal_number", staticVehicleData.getBasic().getInternalNumber());
        vehicleCore.addProperty("contract_type", staticVehicleData.getBasic().getContractType());
        vehicleCore.addProperty("insurance_type", staticVehicleData.getBasic().getInsuranceType());
        vehicleCore.addProperty("acriss_code", staticVehicleData.getBasic().getAcrissCode());

        // Create vehicle technical spec
        JsonObject vehicleTechnicalSpec = new JsonObject();
        vehicleTechnicalSpec.addProperty("capacity_liter", staticVehicleData.getTechnical().getCapacityLiter());
        vehicleTechnicalSpec.addProperty("consumption", staticVehicleData.getTechnical().getConsumption());
        vehicleTechnicalSpec.addProperty("model_year", staticVehicleData.getTechnical().getModelYear());
        vehicleTechnicalSpec.addProperty("manufacturers_code", staticVehicleData.getTechnical().getManufacturersCode());
        vehicleTechnicalSpec.addProperty("gears", staticVehicleData.getTechnical().getGears());
        vehicleTechnicalSpec.addProperty("plug_in_hybrid", staticVehicleData.getTechnical().getPlugInHybrid());
        vehicleTechnicalSpec.addProperty("steering", staticVehicleData.getTechnical().getSteering());
        vehicleTechnicalSpec.addProperty("speedometer", staticVehicleData.getTechnical().getSpeedometer());
        vehicleTechnicalSpec.addProperty("tank_volume", String.valueOf(staticVehicleData.getTechnical().getTankVolume()));
        vehicleTechnicalSpec.addProperty("tank_unit", staticVehicleData.getTechnical().getTankUnit());
        if (StringUtils.isNotBlank(staticVehicleData.getTechnical().getTankVolume2())) {
            if (Float.valueOf(staticVehicleData.getTechnical().getTankVolume2()) > 0.0f) {
                vehicleTechnicalSpec.addProperty("tank_volume2", String.valueOf(staticVehicleData.getTechnical().getTankVolume2()));
            }
        }
        vehicleTechnicalSpec.addProperty("tank_unit2", staticVehicleData.getTechnical().getTankUnit2());
        vehicleTechnicalSpec.addProperty("emission_class", staticVehicleData.getTechnical().getEmissionClass());

        // Create vehicle interior
        JsonObject vehicleInterior = new JsonObject();
        vehicleInterior.addProperty("color", staticVehicleData.getInterior().getColor());
        vehicleInterior.addProperty("upholstery", staticVehicleData.getInterior().getUpholstery());
        vehicleInterior.addProperty("upholstery_color_base", staticVehicleData.getInterior().getUpholsteryColorBase());
        vehicleInterior.addProperty("upholstery_color_type", staticVehicleData.getInterior().getUpholsteryColorType());
        vehicleInterior.addProperty("upholstery_color_description", staticVehicleData.getInterior().getUpholsteryColorDescription());

        // Create vehicle exterior
        JsonObject vehicleExterior = new JsonObject();
        vehicleExterior.addProperty("painting", staticVehicleData.getExterior().getPaint());
        vehicleExterior.addProperty("painting_color_base", staticVehicleData.getExterior().getPaintColorBase());
        vehicleExterior.addProperty("painting_color_description", staticVehicleData.getExterior().getPaintColorDescription());
        vehicleExterior.addProperty("painting_color2", staticVehicleData.getExterior().getPaintColor2());
        vehicleExterior.addProperty("external_color_type", staticVehicleData.getExterior().getExternalColorType());
        vehicleExterior.addProperty("image_url", staticVehicleData.getExterior().getImageUrl());

        // Create vehicle registration
        JsonObject vehicleRegistration = new JsonObject();
        vehicleRegistration.addProperty("holder", staticVehicleData.getRegistration().getHolder());
        vehicleRegistration.addProperty("permit_service", staticVehicleData.getRegistration().getPermitService());

        // Create vehicle contract
        JsonObject vehicleContract = new JsonObject();
        vehicleContract.addProperty("holding_period", staticVehicleData.getContract().getHoldingPeriod());
        vehicleContract.addProperty("mileage", staticVehicleData.getContract().getMileage());
        vehicleContract.addProperty("sixt_fuel_type", staticVehicleData.getContract().getSixtFuelType());

        // Create vehicle equipment
        JsonObject vehicleEquipment = new JsonObject();
        vehicleEquipment.addProperty("meta_charge_cable", staticVehicleData.getEquipment().getChargeCable());
        vehicleEquipment.addProperty("meta_digi_tacho", staticVehicleData.getEquipment().getDigiTacho());
        vehicleEquipment.addProperty("meta_draw_bar", staticVehicleData.getEquipment().getDrawBar());
        vehicleEquipment.addProperty("meta_green_emission_sticker", staticVehicleData.getEquipment().getGreenEmissionSticker());
        vehicleEquipment.addProperty("meta_navigation", staticVehicleData.getEquipment().getNavigation());
        vehicleEquipment.addProperty("meta_navigation_type", staticVehicleData.getEquipment().getNavigationType());
        vehicleEquipment.addProperty("meta_remote_control_parking_heater", staticVehicleData.getEquipment().getRemoteControlParkingHeater());
        vehicleEquipment.addProperty("meta_spare_wheel", staticVehicleData.getEquipment().getSpareWheel());
        vehicleEquipment.addProperty("meta_spare_wheel_type", staticVehicleData.getEquipment().getSpareWheelType());
        vehicleEquipment.addProperty("meta_summer_tires_sticker", staticVehicleData.getEquipment().getSummerTiresSticker());
        vehicleEquipment.addProperty("meta_trunk_cover", staticVehicleData.getEquipment().getTrunkCover());
        vehicleEquipment.addProperty("meta_parcel_shelf", staticVehicleData.getEquipment().getParcelShelf());
        vehicleEquipment.addProperty("meta_vignette", staticVehicleData.getEquipment().getVignette());
        vehicleEquipment.addProperty("meta_vignette_type", staticVehicleData.getEquipment().getVignetteType());
        vehicleEquipment.addProperty("meta_vignette_number", staticVehicleData.getEquipment().getVignetteNumber());
        vehicleEquipment.addProperty("meta_winter_suitable_tyres", staticVehicleData.getEquipment().getWinterSuitableTires());
        vehicleEquipment.addProperty("meta_winter_suitable_tyres_type", staticVehicleData.getEquipment().getWinterSuitableTiresType());
        vehicleEquipment.addProperty("meta_drivers_cab", staticVehicleData.getEquipment().getDriversCab());
        vehicleEquipment.addProperty("meta_lifting_ramp", staticVehicleData.getEquipment().getLiftingRamp());
        vehicleEquipment.addProperty("meta_bluetooth", staticVehicleData.getEquipment().getBluetooth());
        vehicleEquipment.addProperty("meta_barcode", staticVehicleData.getEquipment().getBarcode());
        vehicleEquipment.addProperty("meta_ad_blue", staticVehicleData.getEquipment().getAdBlue());
        vehicleEquipment.addProperty("meta_pollution_badge", staticVehicleData.getEquipment().getPollutionBadge());
        vehicleEquipment.addProperty("list_price_including_equipment", staticVehicleData.getEquipment().getListPriceIncludingEquipment());
        vehicleEquipment.addProperty("meta_customs_cleared", staticVehicleData.getEquipment().getCustomsCleared());
        vehicleEquipment.addProperty("meta_lasi_system", staticVehicleData.getEquipment().getLasiSystem());
        vehicleEquipment.addProperty("meta_lasi_system_type", staticVehicleData.getEquipment().getLasiSystemType());
        vehicleEquipment.addProperty("meta_keyless_go", staticVehicleData.getEquipment().getIsKeyless());

        // Create vehicle invoice
        JsonObject vehicleInvoice = new JsonObject();
        vehicleInvoice.addProperty("currency_code", staticVehicleData.getInvoice().getCurrencyCode());

        // Create vehicle leasing
        JsonObject vehicleLeasing = new JsonObject();
        vehicleLeasing.addProperty("currency_code", staticVehicleData.getLeasing().getCurrencyCode());

        // Create vehicle labels
        JsonObject vehicleLabels = new JsonObject();

        JsonObject vehicleLabelsCore = new JsonObject();
        vehicleLabelsCore.addProperty("make", Make.getEnum(staticVehicleData.getBasic().getMake()).getMakeEnglishDefault());
        vehicleLabelsCore.addProperty("model", Model.getEnum(staticVehicleData.getBasic().getModel()).getModelEnglishDefault());
        vehicleLabelsCore.addProperty("transmission_type", Transmission.getEnum(staticVehicleData.getBasic().getTransmissionType()).getTransmissionEnglishDefault());
        vehicleLabelsCore.addProperty("body_type", BodyType.getEnum(staticVehicleData.getBasic().getBodyType()).getBodyTypeEnglishDefault());

        JsonObject vehicleLabelsExterior = new JsonObject();
        vehicleLabelsExterior.addProperty("painting", "matt");
        vehicleLabelsExterior.addProperty("painting_color_base", Color.getEnum(staticVehicleData.getExterior().getPaintColorBase()).getColorEnglishDefault());

        vehicleLabels.add("vehicle", vehicleLabelsCore);
        vehicleLabels.add("vehicle_exterior", vehicleLabelsExterior);

        // Create event
        JsonObject jsonBody = new JsonObject();
        jsonBody.add("meta", meta);
        jsonBody.addProperty("vehicle_definition_number", staticVehicleData.getVehicleId());
        jsonBody.add("vehicle", vehicleCore);
        jsonBody.add("vehicle_technical_spec", vehicleTechnicalSpec);
        jsonBody.add("vehicle_interior", vehicleInterior);
        jsonBody.add("vehicle_exterior", vehicleExterior);
        jsonBody.add("vehicle_registration", vehicleRegistration);
        jsonBody.add("vehicle_contract", vehicleContract);
        jsonBody.add("vehicle_equipment", vehicleEquipment);
        jsonBody.add("vehicle_invoice", vehicleInvoice);
        jsonBody.add("vehicle_leasing", vehicleLeasing);
        jsonBody.add("vehicle_label", vehicleLabels);

        return jsonBody;
    }

}
