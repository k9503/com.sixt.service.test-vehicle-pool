package com.sixt.service.test_vehicle_pool.application.dto;

import com.google.gson.JsonObject;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public final class FuelTank {
    private String type;
    private int percentage;
    private String filterKey;

    private boolean needsRefueling;

    public String getFilterKey() {
        return filterKey;
    }

    public void setFilterKey(String filterKey) {
        this.filterKey = filterKey;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isNeedsRefueling() {
        return needsRefueling;
    }

    public void setNeedsRefueling(boolean needsRefueling) {
        this.needsRefueling = needsRefueling;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof FuelTank)) return false;

        FuelTank fuelTank = (FuelTank) o;

        return new EqualsBuilder()
                //.append(percentage, fuelTank.percentage) // TODO: is it necesarry to have the same percentage value to express that two fueltype are the equal?
                .append(type, fuelTank.type)
                .append(filterKey, fuelTank.filterKey)
                //.append(needsRefueling, fuelTank.needsRefueling) TODO: disable until an agreement is reached for fuel level threshold
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(type)
                .append(percentage)
                .append(filterKey)
                .append(needsRefueling)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("type", type)
                .append("percentage", percentage)
                .append("filterKey", filterKey)
                .append("needsRefueling", needsRefueling)
                .toString();
    }

    public JsonObject toJson() {
        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("type", type);
        jsonBody.addProperty("percentage", percentage);
        jsonBody.addProperty("filterKey", filterKey);
        jsonBody.addProperty("needsRefueling", needsRefueling);
        return jsonBody;
    }
}
