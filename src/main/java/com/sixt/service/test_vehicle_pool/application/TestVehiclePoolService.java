package com.sixt.service.test_vehicle_pool.application;

import com.sixt.service.salesboost_branch_vehicle_availability.api.VehicleDetailResponse;
import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.Transmission;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import com.sixt.service.test_vehicle_pool.domain.*;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.FastlaneVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.RacTelematicVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.SacVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehicledeletion.VehicleCleaner;
import com.sixt.service.test_vehicle_pool.infrastructure.SalesboostBranchVehicleAvailabilityServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePOJPARepository;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePoolPOJPARepository;
import com.sixt.service.test_vehicle_pool.utils.UUIDCreator;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.List;
import java.util.Optional;

import static net.logstash.logback.marker.Markers.append;

@Service
public class TestVehiclePoolService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestVehiclePoolService.class);

    private SacVehicleCreator sacVehicleCreator;

    private RacTelematicVehicleCreator racTelematicVehicleCreator;

    private FastlaneVehicleCreator fastlaneVehicleCreator;

    private VehicleCleaner vehicleCleaner;

    private ExecutorService executor;

    private VehiclePOJPARepository vehiclePORepository;

    private VehiclePoolPOJPARepository vehiclePoolPORepository;

    private TestVehicleDataCollector testVehicleDataCollector;

    private SalesboostBranchVehicleAvailabilityServiceProxy salesboostBranchVehicleAvailabilityServiceProxy;

    public TestVehiclePoolService(SacVehicleCreator sacVehicleCreator, RacTelematicVehicleCreator racTelematicVehicleCreator,
                                  FastlaneVehicleCreator fastlaneVehicleCreator, VehicleCleaner vehicleCleaner, VehiclePOJPARepository vehiclePORepository,
                                  VehiclePoolPOJPARepository vehiclePoolPORepository, TestVehicleDataCollector testVehicleDataCollector,
                                  SalesboostBranchVehicleAvailabilityServiceProxy salesboostBranchVehicleAvailabilityServiceProxy, ExecutorService executor) {

        this.sacVehicleCreator = sacVehicleCreator;
        this.vehicleCleaner = vehicleCleaner;
        this.racTelematicVehicleCreator = racTelematicVehicleCreator;
        this.fastlaneVehicleCreator = fastlaneVehicleCreator;
        this.executor = executor;
        this.vehiclePORepository = vehiclePORepository;
        this.vehiclePoolPORepository = vehiclePoolPORepository;
        this.testVehicleDataCollector = testVehicleDataCollector;
        this.salesboostBranchVehicleAvailabilityServiceProxy = salesboostBranchVehicleAvailabilityServiceProxy;
        LOGGER.info("Starting test-vehicle-pool service ...");
    }

    @Retryable(value = org.springframework.orm.ObjectOptimisticLockingFailureException.class)
    @Transactional
    public TestVehicle getVehicleFromPool(VehicleType vehicleType, int branchId, AcrissCode acrissCode, String testCaseId) {
        boolean vehicleAvailableClassic = false;
        Optional<VehiclePO> vehicleOptional = vehiclePORepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                branchId, acrissCode.name());

        // Check availability on SixtClassic (not for Fastlane)
        if (vehicleType != VehicleType.FASTLANE_AUTOMATION) {
            while (!vehicleAvailableClassic && vehicleOptional.isPresent()) {
                String vehicleId = vehicleOptional.get().getId();
                VehicleDetailResponse vehicleDetailResponse = salesboostBranchVehicleAvailabilityServiceProxy.getStatusByVehicleId(vehicleId);
                if (vehicleDetailResponse.getDynamic().getVehicleAvailabilityStatus().equals("AVAILABLE")) {
                    vehicleAvailableClassic = true;
                    LOGGER.info(append("vehicleId", vehicleId), "RAC vehicle still available in Sixt Classic");
                } else {
                    LOGGER.info(append("vehicleId", vehicleId), "RAC vehicle not longer available in Sixt Classic. Currenty state: {}",
                            vehicleDetailResponse.getDynamic().getVehicleAvailabilityStatus());

                    // Block the vehicle and get new vehicle from the pool
                    blockTestVehicle(vehicleOptional, "NOT_AVAILABLE_IN_CLASSIC");
                    vehicleOptional = vehiclePORepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                            branchId, acrissCode.name());
                }
            }
        }

        return getAndBlockTestVehicle(vehicleOptional, testCaseId);
    }

    @Retryable(value = org.springframework.orm.ObjectOptimisticLockingFailureException.class)
    @Transactional
    public TestVehicle getVehicleFromPool(VehicleType vehicleType, String testCaseId, Tenant tenant) {
        Optional<VehiclePoolPO> vehiclePoolPOOptional = vehiclePoolPORepository.findByTenantAndVehicleType(tenant.name(),
                vehicleType.name());

        if (!vehiclePoolPOOptional.isPresent()) {
            throw new UndefinedTenantException();
        }

        VehiclePoolPO vehiclePoolPO = vehiclePoolPOOptional.get();
        Optional<VehiclePO> vehicleOptional = vehiclePORepository.findFirstByVehicleTypeAndVehiclePoolIdAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                vehiclePoolPO.getId());

        return getAndBlockTestVehicle(vehicleOptional, testCaseId);
    }

    public long getNumAvailableVehiclesInPool(String vehiclePoolId) {
        return vehiclePORepository.countByVehiclePoolIdAndBlockedFalse(vehiclePoolId);
    }

    public long getNumBlockedVehiclesInPool(String vehiclePoolId) {
        return vehiclePORepository.countByVehiclePoolIdAndBlockedTrue(vehiclePoolId);
    }

    @Transactional
    public TestVehicle lookupVehicleFromPool(String vehicleId) {
        Optional<VehiclePO> vehicleOptional = vehiclePORepository.findById(vehicleId);
        if (vehicleOptional.isPresent()) {
            return testVehicleDataCollector.getCurrentTestVehicleData(vehicleOptional.get().getId());
        } else {
            return null;
        }
    }

    @Transactional
    public CompletableFuture asyncCreateManSacVehicle(VehicleType vehicleType, String vehicleId, String vin, TestLocation testLocation,
                                                      VehicleFuelType vehicleFuelType, boolean isKeyless, Transmission transmission) {
        return CompletableFuture.runAsync(() -> {
            TestVehicle testVehicle = sacVehicleCreator.createSacVehicleManual(vehicleType, vehicleId, vin, testLocation, vehicleFuelType, isKeyless, transmission);

            if (testVehicle != null) {
                VehiclePO vehicle = VehiclePO.createNew(testVehicle.getStaticVehicleData().getVehicleId());
                vehicle.newVehicle(testVehicle);

                vehiclePORepository.save(vehicle);
            }
        }, executor);
    }

    @Transactional
    public BasicRacVehicle asyncCreateManRacTelematicVehicle(int branchId, AcrissCode acrissCode, VehicleFuelType vehicleFuelType,
                                                             boolean withWinterTyres) {
        BasicRacVehicle basicRacVehicle = racTelematicVehicleCreator.createRacTelematicVehicleManualBasic(branchId, acrissCode, vehicleFuelType, withWinterTyres);

        if (basicRacVehicle == null) {
            return null;
        }

        CompletableFuture.runAsync(() -> {
            TestVehicle testVehicle = racTelematicVehicleCreator.finishCreateRacTelematicVehicleManual(basicRacVehicle);

            if (testVehicle != null) {
                VehiclePO vehicle = VehiclePO.createNew(testVehicle.getStaticVehicleData().getVehicleId());
                vehicle.newVehicle(testVehicle);

                vehiclePORepository.save(vehicle);
            }
        }, executor);

        return basicRacVehicle;
    }

    @Transactional
    public BasicRacVehicle asyncCreateManFastlaneVehicle(int branchId, AcrissCode acrissCode, VehicleFuelType vehicleFuelType,
                                                         boolean withWinterTyres, boolean fourWheels, boolean trailerCoupling) {
        BasicRacVehicle basicRacVehicle = fastlaneVehicleCreator.createFastlaneVehicleManualBasic(branchId, acrissCode, vehicleFuelType,
                withWinterTyres, fourWheels, trailerCoupling);

        if (basicRacVehicle == null) {
            return null;
        }

        CompletableFuture.runAsync(() -> {
            TestVehicle testVehicle = fastlaneVehicleCreator.finishCreateFastlaneVehicleManual(basicRacVehicle);

            if (testVehicle != null) {
                VehiclePO vehicle = VehiclePO.createNew(testVehicle.getStaticVehicleData().getVehicleId());
                vehicle.newVehicle(testVehicle);

                vehiclePORepository.save(vehicle);
            }
        }, executor);

        return basicRacVehicle;
    }

    @Transactional
    public boolean deleteManualVehicle(String vehicleId) {
        Optional<VehiclePO> vehicleOptional = vehiclePORepository.findById(vehicleId);

        if (vehicleOptional.isPresent()) {
            VehiclePO vehiclePO = vehicleOptional.get();
            boolean successDeleted = vehicleCleaner.deleteVehicle(vehiclePO.getVehicleType(), vehicleId);
            if (successDeleted) {
                // Delete from test-vehicle-pool DB
                vehiclePORepository.deleteById(vehicleId);
                LOGGER.info(append("vehicleId", vehicleId), "Deleted manual test vehicle");
            } else {
                LOGGER.error(append("vehicleId", vehicleId), "Deletion of manual test vehicle failed");
                return false;
            }
        } else {
            LOGGER.debug(append("vehicleId", vehicleId), "Manual test vehicle was already deleted");
        }

        return true;
    }

    @Transactional
    public void clearVehiclePool(String vehiclePoolId) {
        List<VehiclePO> vehiclePOList = vehiclePORepository.findByVehiclePoolId(vehiclePoolId);

        for (VehiclePO vehiclePO : vehiclePOList) {
            vehiclePO.block("CLEAR_POOL");
            vehiclePORepository.save(vehiclePO);
        }
    }

    @Transactional
    public String createVehiclePool(VehiclePool vehiclePool) {
        String vehiclePoolId;
        if (vehiclePool.getId().isEmpty()) {
            vehiclePoolId = UUIDCreator.createRandomE2EUUID();
        } else {
            vehiclePoolId = vehiclePool.getId();
        }

        VehiclePoolPO vehiclePoolPO = VehiclePoolPO.createNew(vehiclePoolId);
        vehiclePoolPO.newPool(vehiclePool);

        vehiclePoolPORepository.save(vehiclePoolPO);

        return vehiclePoolId;
    }

    @Transactional
    public String updateVehiclePool(VehiclePool vehiclePool) {
        Optional<VehiclePoolPO> vehiclePoolPOOptional = vehiclePoolPORepository.findById(vehiclePool.getId());
        if (vehiclePoolPOOptional.isPresent()) {
            VehiclePoolPO updateVehiclePoolPO = vehiclePoolPOOptional.get();
            updateVehiclePoolPO.updatePool(vehiclePool);
            vehiclePoolPORepository.save(updateVehiclePoolPO);

            return updateVehiclePoolPO.getId();
        } else {
            LOGGER.error("Update of vehicle pool failed. Pool not found for vehicle pool id {}", vehiclePool.getId());
            return null;
        }
    }

    @Transactional
    public List<VehiclePool> getAllVehiclePools() {
        List<VehiclePoolPO> vehiclePoolPOList = vehiclePoolPORepository.findAll();
        List<VehiclePool> vehiclePools = new ArrayList<>();

        for (VehiclePoolPO vehiclePoolPO : vehiclePoolPOList) {
            vehiclePools.add(vehiclePoolPO.getVehiclePool());
        }

        return vehiclePools;
    }

    @Transactional
    public VehiclePool getVehiclePool(String vehiclePoolId) {
        Optional<VehiclePoolPO> vehiclePoolPOOptional = vehiclePoolPORepository.findById(vehiclePoolId);

        if (vehiclePoolPOOptional.isPresent()) {
            return vehiclePoolPOOptional.get().getVehiclePool();
        }

        return null;
    }

    @Transactional
    public void deleteVehiclePool(String vehiclePoolId) {
        Optional<VehiclePoolPO> vehiclePoolPOOptional = vehiclePoolPORepository.findById(vehiclePoolId);

        if (vehiclePoolPOOptional.isPresent()) {
            vehiclePoolPORepository.deleteById(vehiclePoolId);
            LOGGER.info("Deleted vehicle pool with id {}", vehiclePoolId);
        } else {
            LOGGER.debug("Vehicle pool was already deleted");
        }
    }

    @Transactional
    public void deleteVehiclesFromPool(String vehiclePoolId) {
        List<VehiclePO> vehiclePOList = vehiclePORepository.findByVehiclePoolId(vehiclePoolId);

        LOGGER.info("Clean {} test vehicles from pool with id <{}>", vehiclePOList.size(), vehiclePoolId);

        for (VehiclePO currentVehiclePO : vehiclePOList) {
            boolean successDeleted = vehicleCleaner.deleteVehicle(currentVehiclePO.getVehicleType(), currentVehiclePO.getId());

            if (successDeleted) {
                vehiclePORepository.delete(currentVehiclePO);
                LOGGER.debug(append("vehicleId", currentVehiclePO.getId()), "Deleted test vehicle successfully");
            } else {
                LOGGER.error(append("vehicleId", currentVehiclePO.getId()), "Deletion of test vehicle failed");
            }

        }
        LOGGER.info("Cleaned up test vehicles from pool with id {}", vehiclePoolId);
    }

    @Transactional
    public List<AvailableVehicle> getAvailableSacVehiclesByLocation(TestLocation testLocation) {
        List<AvailableVehicle> availableVehiclesList = new ArrayList<>();

        List<VehiclePO> vehiclePOSacManualList = vehiclePORepository.findByVehicleTypeAndTestLocationAndBlockedFalse(VehicleType.SAC_MANUAL.name(),
                testLocation.name());

        for (VehiclePO currentVehiclePO : vehiclePOSacManualList) {
            addAvailableVehicle(availableVehiclesList, currentVehiclePO);
        }

        List<VehiclePO> vehiclePOSacSimulatorList = vehiclePORepository.findByVehicleTypeAndTestLocationAndBlockedFalse(VehicleType.SAC_SIMULATOR.name(),
                testLocation.name());

        for (VehiclePO currentVehiclePO : vehiclePOSacSimulatorList) {
            addAvailableVehicle(availableVehiclesList, currentVehiclePO);
        }

        return availableVehiclesList;
    }

    private void blockTestVehicle(Optional<VehiclePO> vehicleOptional, String blockReason) {
        if (vehicleOptional.isPresent()) {
            VehiclePO vehiclePO = vehicleOptional.get();
            vehiclePO.block(blockReason);

            vehiclePORepository.save(vehiclePO);
            LOGGER.info(append("vehicleId", vehiclePO.getId()), "Blocked test vehicle");
        } else {
            LOGGER.error("Test vehicle could not be blocked");
        }
    }

    @Nullable
    private TestVehicle getAndBlockTestVehicle(Optional<VehiclePO> vehicleOptional, String testCaseId) {
        if (!vehicleOptional.isPresent()) {
            throw new OutOfTestVehicleException();
        }

        VehiclePO vehicle = vehicleOptional.get();
        vehicle.block(testCaseId);

        vehiclePORepository.save(vehicle);

        return testVehicleDataCollector.getCurrentTestVehicleStaticData(vehicle.getId());
    }

    private void addAvailableVehicle(List<AvailableVehicle> availableVehiclesList, VehiclePO currentVehiclePO) {
        AvailableVehicle availableVehicle = AvailableVehicle.newBuilder().
                setVehicleId(currentVehiclePO.getId()).
                setVin(currentVehiclePO.getVin()).
                setAcrissCode(currentVehiclePO.getAcrissCode()).
                setVehicleType(currentVehiclePO.getVehicleType()).build();
        availableVehiclesList.add(availableVehicle);
    }

}
