package com.sixt.service.test_vehicle_pool.application.dto;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class DynamicVehicleData {
    private boolean available;
    private boolean deleted;
    private String ignitionState;
    private String lockState;
    @SerializedName(value = "position", alternate = "location")
    private VehiclePosition position;
    private VehicleAddress pickupAddress;
    private int odometer;
    private int chargeLevel;
    private int fuelLevelTotal;
    private VehiclePrice drivingPrice;
    private VehiclePrice resExtendPrice;
    private int branchId;

    public void setIgnitionState(String ignitionState) {
        this.ignitionState = ignitionState;
    }

    public void setLockState(String lockState) {
        this.lockState = lockState;
    }

    public VehiclePrice getDrivingPrice() {
        return drivingPrice;
    }

    public void setDrivingPrice(VehiclePrice drivingPrice) {
        this.drivingPrice = drivingPrice;
    }

    public VehiclePrice getResExtendPrice() {
        return resExtendPrice;
    }

    public void setResExtendPrice(VehiclePrice resExtendPrice) {
        this.resExtendPrice = resExtendPrice;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getIgnitionState() {
        return ignitionState;
    }

    public void setIgnitionState(VehicleIgnitionState vehicleIgnitionState) {
        this.ignitionState = vehicleIgnitionState.toString();
    }

    public String getLockState() {
        return lockState;
    }

    public void setLockState(VehicleLockState vehicleLockState) {
        this.lockState = vehicleLockState.toString();
    }

    public VehiclePosition getPosition() {
        return position;
    }

    @JsonSetter("position")
    public void setPosition(VehiclePosition position) {
        this.position = position;
    }

    @JsonSetter("location")
    public void setLocation(VehiclePosition location) {
        if (this.position == null) {
            this.position = location;
        }
    }

    public VehicleAddress getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(VehicleAddress pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public void increaseOdometerBy(int additionalMileage) {
        odometer += additionalMileage;
    }

    public int getChargeLevel() {
        return chargeLevel;
    }

    public void setChargeLevel(int chargeLevel) {
        this.chargeLevel = chargeLevel;
    }

    public int getFuelLevelTotal() {
        return fuelLevelTotal;
    }

    public void setFuelLevelTotal(int fuelLevelTotal) {
        this.fuelLevelTotal = fuelLevelTotal;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DynamicVehicleData that = (DynamicVehicleData) o;

        return new EqualsBuilder()
                .append(available, that.available)
                .append(deleted, that.deleted)
                .append(odometer, that.odometer)
                .append(chargeLevel, that.chargeLevel)
                .append(fuelLevelTotal, that.fuelLevelTotal)
                .append(branchId, that.branchId)
                .append(ignitionState, that.ignitionState)
                .append(lockState, that.lockState)
                .append(position, that.position)
                .append(pickupAddress, that.pickupAddress)
                .append(drivingPrice, that.drivingPrice)
                .append(resExtendPrice, that.resExtendPrice)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(available)
                .append(deleted)
                .append(ignitionState)
                .append(lockState)
                .append(position)
                .append(pickupAddress)
                .append(odometer)
                .append(chargeLevel)
                .append(fuelLevelTotal)
                .append(drivingPrice)
                .append(resExtendPrice)
                .append(branchId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("available", available)
                .append("deleted", deleted)
                .append("ignitionState", ignitionState)
                .append("lockState", lockState)
                .append("position", position)
                .append("pickupAddress", pickupAddress)
                .append("odometer", odometer)
                .append("chargeLevel", chargeLevel)
                .append("fuelLevelTotal", fuelLevelTotal)
                .append("drivingPrice", drivingPrice)
                .append("resExtendPrice", resExtendPrice)
                .append("branchId", branchId)
                .toString();
    }

}
