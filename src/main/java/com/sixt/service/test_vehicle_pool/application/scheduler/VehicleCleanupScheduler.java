package com.sixt.service.test_vehicle_pool.application.scheduler;

import com.sixt.service.test_vehicle_pool.application.ScheduledTasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class VehicleCleanupScheduler {

    @Autowired
    private ScheduledTasks scheduledTasks;

    /**
     * Scheduler to cleanup test vehicles of the different types from DB which were already deleted
     * and are not longer needed.
     * <p>
     * Run this scheduler every hour / 3600 sec
     */
    @Scheduled(fixedRate = 3600000)
    public void vehicleCleanupScheduler() {
        scheduledTasks.vehicleCleanup();
    }

}
