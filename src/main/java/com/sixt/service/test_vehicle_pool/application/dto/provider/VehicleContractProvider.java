package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.fleet.api.VehicleV2.Contract;

public class VehicleContractProvider {
    private final static int DEFAULT_HOLDING_PERIOD = 12;
    private final static int DEFAULT_MILEAGE = 3456;
    private final static String DEFAULT_FUEL_TYPE = "Gas";

    public static Contract createDefaultContract() {
        return Contract.newBuilder()
                .setHoldingPeriod(DEFAULT_HOLDING_PERIOD)
                .setMileage(DEFAULT_MILEAGE)
                .setSixtFuelType(DEFAULT_FUEL_TYPE)
                .build();
    }

}