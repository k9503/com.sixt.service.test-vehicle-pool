package com.sixt.service.test_vehicle_pool.application;

public class HttpRequestFailedException extends RuntimeException {

    public HttpRequestFailedException(String errorMessage) {
        super(errorMessage);
    }
}