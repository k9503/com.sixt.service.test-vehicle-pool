package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.fleet.api.VehicleV2.Invoice;

public class VehicleInvoiceProvider {
    private final static String DEFAULT_CURRENCY_CODE = "EUR";

    public static Invoice createDefaultInvoice() {
        return Invoice.newBuilder()
                .setCurrencyCode(DEFAULT_CURRENCY_CODE)
                .build();
    }

}