package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.DeleteVehiclePoolRequest;
import com.sixt.service.test_vehicle_pool.api.DeleteVehiclePoolResponse;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

@Component
@RpcHandler("TestVehiclePool.DeleteVehiclePool")
public class DeleteVehiclePoolHandler implements ServiceMethodHandler<DeleteVehiclePoolRequest,
        DeleteVehiclePoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteVehiclePoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public DeleteVehiclePoolResponse handleRequest(DeleteVehiclePoolRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling DeleteVehiclePool");

        try {
            // Remove pool vehicles before deleting the pool
            service.deleteVehiclesFromPool(request.getVehiclePoolId());

            // Delete the pool itself
            service.deleteVehiclePool(request.getVehiclePoolId());
        } catch (Exception e) {
            LOGGER.error("Failed to delete vehicle pool with id {}: {}", request.getVehiclePoolId(), e.getMessage());
            throw new RpcCallException(RpcCallException.Category.InternalServerError, "Delete vehicle pool failed");
        }

        LOGGER.info("Successfully deleted vehicle pool with id {}", request.getVehiclePoolId());

        return DeleteVehiclePoolResponse.newBuilder()
                .build();
    }

}
