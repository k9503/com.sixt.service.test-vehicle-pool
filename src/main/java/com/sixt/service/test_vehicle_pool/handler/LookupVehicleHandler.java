package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.LookupVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.LookupVehicleResponse;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static com.sixt.service.test_vehicle_pool.api.LookupVehicleResponse.Error.NOT_FOUND;
import static net.logstash.logback.marker.Markers.append;

@Component
@RpcHandler("TestVehiclePool.LookupVehicle")
public class LookupVehicleHandler implements ServiceMethodHandler<LookupVehicleRequest, LookupVehicleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LookupVehicleHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public LookupVehicleResponse handleRequest(LookupVehicleRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info(append("vehicleId", request.getVehicleId()), "Handling LookupVehicle");

        TestVehicle testVehicle = service.lookupVehicleFromPool(request.getVehicleId());

        if (testVehicle == null) {
            throw new RpcCallException(RpcCallException.Category.ResourceNotFound, "Test vehicle not found or could not get complete data for test vehicle")
                    .withErrorCode(NOT_FOUND.name());
        }

        LOGGER.info(append("vehicleId", request.getVehicleId()), "Successful lookup of vehicle");

        return LookupVehicleResponse.newBuilder()
                .setVehicleType(testVehicle.getVehicleType())
                .setVin(testVehicle.getStaticVehicleData().getBasic().getVin())
                .setTcId(testVehicle.getTcId())
                .setAcrissCode(testVehicle.getStaticVehicleData().getBasic().getAcrissCode())
                .setBranchId(testVehicle.getDynamicVehicleData().getBranchId())
                .setVehiclePoolId(testVehicle.getVehiclePoolId()).build();
    }

}