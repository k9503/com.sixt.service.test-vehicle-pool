package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.ClearVehiclePoolRequest;
import com.sixt.service.test_vehicle_pool.api.ClearVehiclePoolResponse;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

@Component
@RpcHandler("TestVehiclePool.ClearVehiclePool")
public class ClearVehiclePoolHandler implements ServiceMethodHandler<ClearVehiclePoolRequest, ClearVehiclePoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClearVehiclePoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public ClearVehiclePoolResponse handleRequest(ClearVehiclePoolRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling ClearVehiclePool");

        try {
            service.clearVehiclePool(request.getVehiclePoolId());
        } catch (Exception e) {
            LOGGER.error("Failed clearing the pool with id {}", request.getVehiclePoolId());
            throw new RpcCallException(RpcCallException.Category.InternalServerError, "Clearing vehicle pool failed");
        }

        LOGGER.info("Successfully cleared vehicle pool with id {}", request.getVehiclePoolId());

        return ClearVehiclePoolResponse.getDefaultInstance();
    }

}
