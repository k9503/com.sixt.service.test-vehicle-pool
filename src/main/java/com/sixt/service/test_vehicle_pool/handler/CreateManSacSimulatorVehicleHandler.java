package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.CreateManSacSimulatorVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.CreateManSacSimulatorVehicleResponse;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.Transmission;
import com.sixt.service.test_vehicle_pool.application.dto.VinGenerator;
import com.sixt.service.test_vehicle_pool.utils.UUIDCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static net.logstash.logback.marker.Markers.append;

@Component
@RpcHandler("TestVehiclePool.CreateManSacSimulatorVehicle")
public class CreateManSacSimulatorVehicleHandler implements ServiceMethodHandler<CreateManSacSimulatorVehicleRequest, CreateManSacSimulatorVehicleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateManSacSimulatorVehicleHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public CreateManSacSimulatorVehicleResponse handleRequest(CreateManSacSimulatorVehicleRequest request, OrangeContext orangeContext) throws RpcCallException {
        LOGGER.info("Handling CreateManSacSimulatorVehicle");

        String vehicleId = UUIDCreator.createRandomE2EUUID();
        String vin = VinGenerator.getVinByType(VehicleType.SAC_SIMULATOR);

        Transmission transmission;
        try {
            transmission = Transmission.valueOf(request.getTransmission());
        } catch (Exception e) {
            LOGGER.error("Transmission {} not supported", request.getTransmission());
            throw new RpcCallException(RpcCallException.Category.BadRequest, "Transmission not supported");
        }

        service.asyncCreateManSacVehicle(VehicleType.SAC_SIMULATOR, vehicleId, vin, request.getTestLocation(), request.getVehicleFuelType(), request.getKeyless(),
                transmission);
        LOGGER.info(append("vehicleId", vehicleId), "Triggered CreateManSimulatorSacVehicle");

        return CreateManSacSimulatorVehicleResponse.newBuilder()
                .setVehicleId(vehicleId)
                .setVin(vin)
                .build();
    }

}
