package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.GetNumVehiclesInPoolRequest;
import com.sixt.service.test_vehicle_pool.api.GetNumVehiclesInPoolResponse;
import com.sixt.service.test_vehicle_pool.api.VehicleState;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

@Component
@RpcHandler("TestVehiclePool.GetNumVehiclesInPool")
public class GetNumVehiclesInPoolHandler
        implements ServiceMethodHandler<GetNumVehiclesInPoolRequest, GetNumVehiclesInPoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetNumVehiclesInPoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public GetNumVehiclesInPoolResponse handleRequest(GetNumVehiclesInPoolRequest request, OrangeContext orangeContext) {

        LOGGER.info("Handling GetNumVehiclesInPool");

        VehicleState vehicleState = request.getVehicleState();
        String vehiclePoolId = request.getVehiclePoolId();

        long numVehicles = 0;
        if (vehicleState == VehicleState.AVAILABLE) {
            numVehicles = service.getNumAvailableVehiclesInPool(vehiclePoolId);
            LOGGER.info("There are {} available vehicles in the pool {}", numVehicles, vehiclePoolId);
        }

        if (vehicleState == VehicleState.BLOCKED) {
            numVehicles = service.getNumBlockedVehiclesInPool(vehiclePoolId);
            LOGGER.info("There are {} blocked vehicles in the pool {}", numVehicles, vehiclePoolId);
        }

        return GetNumVehiclesInPoolResponse.newBuilder()
                .setNumVehicles(numVehicles)
                .build();
    }

}
