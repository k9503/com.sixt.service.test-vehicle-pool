package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import java.util.List;

@Component
@RpcHandler("TestVehiclePool.GetAllVehiclePools")
public class GetAllVehiclePoolsHandler implements ServiceMethodHandler<GetAllVehiclePoolsRequest,
        GetAllVehiclePoolsResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetAllVehiclePoolsHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public GetAllVehiclePoolsResponse handleRequest(GetAllVehiclePoolsRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling GetAllVehiclePools");

        List<VehiclePool> vehiclePools;

        try {
            vehiclePools = service.getAllVehiclePools();
        } catch (Exception e) {
            LOGGER.error("Failed to get all vehicle pools: {}", e.getMessage());
            throw new RpcCallException(RpcCallException.Category.InternalServerError, "Get all vehicle pools failed");
        }

        LOGGER.info("Successfully retrieved vehicle pools");

        return GetAllVehiclePoolsResponse.newBuilder()
                .addAllVehiclePool(vehiclePools)
                .build();
    }

}
