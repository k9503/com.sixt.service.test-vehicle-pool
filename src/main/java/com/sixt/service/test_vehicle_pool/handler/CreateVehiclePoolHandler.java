package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.domain.RacVehicleEligibilityCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static com.sixt.service.test_vehicle_pool.api.CreateVehiclePoolResponse.Error.CREATION_FAILED;
import static com.sixt.service.test_vehicle_pool.api.CreateVehiclePoolResponse.Error.NOT_SUPPORTED;

@Component
@RpcHandler("TestVehiclePool.CreateVehiclePool")
public class CreateVehiclePoolHandler implements ServiceMethodHandler<CreateVehiclePoolRequest,
        CreateVehiclePoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateVehiclePoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Autowired
    private RacVehicleEligibilityCheck racVehicleEligibilityCheck;

    @Override
    public CreateVehiclePoolResponse handleRequest(CreateVehiclePoolRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling CreateVehiclePool");

        String vehiclePoolId;
        CentralPosition centralPosition = request.getVehiclePool().getCentralPosition();

        VehicleType vehicleType = request.getVehiclePool().getVehicleType();
        int branchId = request.getVehiclePool().getBranchId();
        String acrissCode = request.getVehiclePool().getAcrissCode();

        // Verify if valid central position is provided for vehicle type SAC_AUTOMATION / SAC_PERFORMANCE
        if (vehicleType == VehicleType.SAC_AUTOMATION || vehicleType == VehicleType.SAC_PERFORMANCE) {
            if (centralPosition == null) {
                LOGGER.error("Central position has to be provided for vehicle type SAC_AUTOMATION / SAC_PERFORMANCE");
                throw new RpcCallException(RpcCallException.Category.BadRequest, "Central point has to be provided for vehicle type SAC_AUTOMATION / SAC_PERFORMANCE");
            } else if (centralPosition.getLat() == 0.0F || centralPosition.getLon() == 0.0F) {
                LOGGER.error("Central position lat / long has to be provided for vehicle type SAC_AUTOMATION / SAC_PERFORMANCE");
                throw new RpcCallException(RpcCallException.Category.BadRequest, "Central point lat / long must be set");
            }
        }

        // Verify if station / acriss code combination is possible for RAC vehicles
        if (vehicleType == VehicleType.RAC_AUTOMATION || vehicleType == VehicleType.RAC_TELEMATIC_AUTOMATION || vehicleType == VehicleType.FASTLANE_AUTOMATION) {
            if (!racVehicleEligibilityCheck.isRacVehicleEligible(branchId, acrissCode)) {
                throw new RpcCallException(RpcCallException.Category.BadRequest, "RAC vehicle can not be created for station (country) / acriss code combination")
                        .withErrorCode(NOT_SUPPORTED.name());
            }
        }

        try {
            vehiclePoolId = service.createVehiclePool(request.getVehiclePool());
        } catch (Exception e) {
            LOGGER.error("Failed to create vehicle pool: {}", e.getMessage());
            throw new RpcCallException(RpcCallException.Category.BackendError, "Test vehicle pool creation failed")
                    .withErrorCode(CREATION_FAILED.name());
        }
        LOGGER.info("Successfully created vehicle pool with id {}", vehiclePoolId);

        VehiclePool createdVehiclePool = service.getVehiclePool(vehiclePoolId);
        if (createdVehiclePool == null) {
            LOGGER.error("Failed to get created vehicle pool");
            throw new RpcCallException(RpcCallException.Category.BackendError, "Could not get new created vehicle pool");
        }

        return CreateVehiclePoolResponse.newBuilder()
                .setVehiclePool(createdVehiclePool)
                .build();
    }

}
