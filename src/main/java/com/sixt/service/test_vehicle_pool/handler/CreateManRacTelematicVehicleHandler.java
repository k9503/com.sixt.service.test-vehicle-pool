package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.CreateManRacTelematicVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.CreateManRacTelematicVehicleResponse;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;


import static net.logstash.logback.marker.Markers.append;

@Component
@RpcHandler("TestVehiclePool.CreateManRacTelematicVehicle")
public class CreateManRacTelematicVehicleHandler implements ServiceMethodHandler<CreateManRacTelematicVehicleRequest,
        CreateManRacTelematicVehicleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateManRacTelematicVehicleHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public CreateManRacTelematicVehicleResponse handleRequest(CreateManRacTelematicVehicleRequest request, OrangeContext orangeContext)
            throws RpcCallException {

        LOGGER.info("Handling CreateManRacTelematicVehicle");

        if (request.getBranchId() != 1) {
            LOGGER.error("Only branch id 1 supported for RacTelematicVehicle");
            throw new RpcCallException(RpcCallException.Category.BadRequest, "Only branch id 1 supported for RacTelematicVehicle");
        }

        BasicRacVehicle basicRacVehicle = service.asyncCreateManRacTelematicVehicle(request.getBranchId(), AcrissCode.valueOf(request.getAcrissCode()),
                request.getVehicleFuelType(), request.getWinterTyres());
        LOGGER.info(append("vehicleId", basicRacVehicle.getVehicleId()), "Triggered CreateManRacTelematicVehicle");

        String countryCode = basicRacVehicle.getBranchObject().getAddresses(0).getCountry().getIso2Code();

        return CreateManRacTelematicVehicleResponse.newBuilder()
                .setVehicleId(basicRacVehicle.getVehicleId())
                .setBranchId(request.getBranchId())
                .setCountry(countryCode)
                .setAcrissCode(request.getAcrissCode())
                .setLicensePlate(basicRacVehicle.getLicensePlate())
                .build();
    }

}
