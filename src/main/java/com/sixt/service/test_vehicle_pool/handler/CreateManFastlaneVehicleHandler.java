package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.CreateManFastlaneVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.CreateManFastlaneVehicleResponse;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import static net.logstash.logback.marker.Markers.append;

@Component
@RpcHandler("TestVehiclePool.CreateManFastlaneVehicle")
public class CreateManFastlaneVehicleHandler implements ServiceMethodHandler<CreateManFastlaneVehicleRequest,
        CreateManFastlaneVehicleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateManFastlaneVehicleHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public CreateManFastlaneVehicleResponse handleRequest(CreateManFastlaneVehicleRequest request, OrangeContext orangeContext) {

        LOGGER.info("Handling CreateManFastlaneVehicle");

        BasicRacVehicle basicRacVehicle = service.asyncCreateManFastlaneVehicle(request.getBranchId(), AcrissCode.valueOf(request.getAcrissCode()),
                request.getVehicleFuelType(), request.getWinterTyres(), request.getFourWheels(), request.getTrailerCoupling());

        LOGGER.info(append("vehicleId", basicRacVehicle.getVehicleId()), "Triggered CreateManFastlaneVehicle");

        String countryCode = basicRacVehicle.getBranchObject().getAddresses(0).getCountry().getIso2Code();

        return CreateManFastlaneVehicleResponse.newBuilder()
                .setVehicleId(basicRacVehicle.getVehicleId())
                .setBranchId(request.getBranchId())
                .setCountry(countryCode)
                .setAcrissCode(request.getAcrissCode())
                .setLicensePlate(basicRacVehicle.getLicensePlate())
                .build();
    }

}
