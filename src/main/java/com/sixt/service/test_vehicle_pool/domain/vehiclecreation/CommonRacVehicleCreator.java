package com.sixt.service.test_vehicle_pool.domain.vehiclecreation;

import com.sixt.service.branch.api.BranchAddress;
import com.sixt.service.branch.api.BranchOuterClass.BranchObject;
import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.EturnApi;
import com.sixt.service.test_vehicle_pool.application.IntegrationFleetApi;
import com.sixt.service.test_vehicle_pool.application.dto.*;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.Country;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.VehicleState;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.RacVehicleProvider;
import com.sixt.service.test_vehicle_pool.infrastructure.BranchServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.FleetServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.OneFakeVehiclesServiceProxy;
import com.sixt.service.test_vehicle_pool.utils.Sleeper;
import com.sixt.testdatamanagement.rac.vehicle.creation.config.TestEnvironment;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.Vehicle;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.VehicleData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommonRacVehicleCreator {

    private final static int DEFAULT_FUEL_LEVEL_TOTAL = 60000;
    private final static int DEFAULT_CHARGE_LEVEL = 100;
    private final static int DEFAULT_ODOMETER = 9999;

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonRacVehicleCreator.class);

    @Autowired
    private FleetServiceProxy fleetServiceProxy;
    @Autowired
    private BranchServiceProxy branchServiceProxy;
    @Autowired
    private OneFakeVehiclesServiceProxy oneFakeVehiclesServiceProxy;
    @Autowired
    private IntegrationFleetApi integrationFleetApi;
    @Autowired
    private RacVehicleCreationStepsInitializer racVehicleCreationStepsInitializer;
    @Autowired
    private EturnApi eturnApi;

    public BasicRacVehicle createCommonRacVehicleBasic(VehicleType vehicleType, int branchId, AcrissCode acrissCode,
                                                       VehicleFuelType vehicleFuelType, boolean withWinterTyres,
                                                       boolean fourWheels, boolean trailerCoupling) {

        BranchObject branch = branchServiceProxy.get(branchId);

        final String licensePlate = LicensePlateGenerator.getLicensePlateByType(vehicleType);
        final String vin = VinGenerator.getVinByType(vehicleType);

        String branchCountryCode = branch.getAddresses(0).getCountry().getIso2Code();
        TestEnvironment testEnvironment = racVehicleCreationStepsInitializer.getTestEnvironment();
        Vehicle legacyVehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.valueOf(branchCountryCode), vin, licensePlate,
                acrissCode, vehicleFuelType, withWinterTyres, fourWheels, trailerCoupling);

        if (legacyVehicle == null) {
            LOGGER.error("Legacy vehicle with type {} and acriss code {} not available on branch {}", vehicleType, acrissCode.name(), branch.getBranchId());
            return null;
        }

        // Create legacy vehicle
        String legacyVehicleId = racVehicleCreationStepsInitializer.getRacVehicleCreationSteps().createVehicle(legacyVehicle);

        if (legacyVehicleId == null) {
            LOGGER.error("RAC vehicle was not created");
            return null;
        }
        LOGGER.info("Basic common RAC vehicle created with id {}", legacyVehicleId);

        return new BasicRacVehicle(legacyVehicleId, legacyVehicle.getCompany(), branch, licensePlate);
    }

    public TestVehicle finalizeCommonRacVehicleCreation(BasicRacVehicle basicRacVehicle, VehicleState vehicleState) {
        TestVehicle testVehicle = new TestVehicle();

        String vehicleId = basicRacVehicle.getVehicleId();
        BranchObject branch = basicRacVehicle.getBranchObject();
        int branchId = (int) branch.getBranchId();

        // Publish legacy vehicle
        boolean publishSuccess = racVehicleCreationStepsInitializer.getRacVehicleCreationSteps().publish(vehicleId, "1", basicRacVehicle.getCompany());
        if (!publishSuccess) {
            LOGGER.error("Publishing of new legacy vehicle failed");
            return null;
        }

        racVehicleCreationStepsInitializer.getRacVehicleCreationSteps().makeVehicleReadyForOne(vehicleId, branchId, vehicleState.getValue());

        VehicleData legacyVehicleData = racVehicleCreationStepsInitializer.getRacVehicleCreationSteps().getVehicleData(vehicleId);

        // Wait until event was received and vehicle was created in ONE fleet
        Sleeper.sleepForSeconds(60);

        // Verify if vehicle is available in the ONE fleet service and was successfully synchronized
        VehicleV2 vehicleV2 = fleetServiceProxy.getByVehicleIdV2(vehicleId);
        if (vehicleV2 == null) {
            LOGGER.error("New created RAC vehicle was not synchronized to the ONE fleet");
            return null;
        }

        testVehicle.setStaticVehicleData(vehicleV2);

        racVehicleCreationStepsInitializer.getRacVehicleCreationSteps().setVehicleData(legacyVehicleData.getInternalNumber(), branchId, vehicleState.getValue(), true, 7, 1);

        BranchAddress.Coordinates coordinates = branch.getLocation().getCoordinates();

        VehiclePosition position = new VehiclePosition(coordinates.getLat(), coordinates.getLon());

        DynamicVehicleData dynamicVehicleData = new DynamicVehicleData();
        dynamicVehicleData.setDrivingPrice(new VehiclePrice(10, 10));
        dynamicVehicleData.setResExtendPrice(new VehiclePrice(10, 10));
        dynamicVehicleData.setPosition(position);
        dynamicVehicleData.setIgnitionState(VehicleIgnitionState.IGNITION_OFF);
        dynamicVehicleData.setLockState(VehicleLockState.LOCKED);
        dynamicVehicleData.setFuelLevelTotal(DEFAULT_FUEL_LEVEL_TOTAL);
        dynamicVehicleData.setChargeLevel(DEFAULT_CHARGE_LEVEL);
        dynamicVehicleData.setOdometer(DEFAULT_ODOMETER);
        dynamicVehicleData.setBranchId(branchId);

        testVehicle.setDynamicVehicleData(dynamicVehicleData);

        try {
            oneFakeVehiclesServiceProxy.setState(testVehicle);
        } catch (Exception e) {
            LOGGER.error("Setting new vehicle state in one-fake-vehicles failed.", e);
            return null;
        }

        return testVehicle;
    }

    public void addTelematicUnit(String vehicleId, String vin) {
        integrationFleetApi.addTelematicUnit(vehicleId, vin);
    }

    public void eturnCheckIn(String vehicleId, String licensePlate) {
        eturnApi.checkIn(vehicleId, licensePlate);
    }


}
