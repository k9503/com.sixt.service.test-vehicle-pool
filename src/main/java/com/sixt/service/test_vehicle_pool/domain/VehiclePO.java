package com.sixt.service.test_vehicle_pool.domain;


import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class VehiclePO implements Persistable<String> {

    @Id
    private String id;

    @Transient
    private boolean isNew = false;

    @Version
    private long version;

    private String vin;

    private String tcId;

    private Instant creationTime;

    private Instant blockedTime;

    private String vehicleType;

    private int branchId;

    private String acrissCode;

    private boolean blocked;

    private String vehiclePoolId;

    private String testLocation;

    private boolean deleted;

    private VehiclePO() {
    } // JPA


    private VehiclePO(String id) {
        this.id = id;
        isNew = true;
    }

    public static VehiclePO createNew(String id) {
        return new VehiclePO(id);
    }

    @Override
    public String getId() {
        return id;
    }

    public void block(String testCaseId) {
        blocked = true;
        blockedTime = Instant.now();
        tcId = testCaseId;
    }

    public void setDeleted() {
        deleted = true;
    }

    public void newVehicle(TestVehicle testVehicle) {
        vin = testVehicle.getStaticVehicleData().getBasic().getVin();
        creationTime = Instant.now();
        vehicleType = testVehicle.getVehicleType().name();
        branchId = testVehicle.getDynamicVehicleData().getBranchId();
        acrissCode = testVehicle.getStaticVehicleData().getBasic().getAcrissCode();
        blocked = false;
        tcId = "";
        vehiclePoolId = testVehicle.getVehiclePoolId();
        testLocation = testVehicle.getTestLocation();
        deleted = false;
    }

    public VehicleType getVehicleType() {
        return VehicleType.valueOf(vehicleType);
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public String getTcId() {
        return tcId;
    }

    public String getVin() {
        return vin;
    }

    public Instant getBlockedTime() {
        return blockedTime;
    }

    public int getBranchId() {
        return branchId;
    }

    public String getAcrissCode() {
        return acrissCode;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public String getVehiclePoolId() { return vehiclePoolId; }

    public String getTestLocation() { return testLocation; }

    public boolean isDeleted() {
        return deleted;
    }

    // Entity state handling - since we use a assigned (never null) identifier, we need to use Persistable
    // to tell the Spring JPARepository that the instance is new (i.e. has not been persisted before).
    // c.f. https://docs.spring.io/spring-data/jpa/docs/2.2.0.RELEASE/reference/html/#jpa.entity-persistence
    @Override
    public boolean isNew() {
        return isNew;
    }

    @PrePersist
    @PostLoad
    void markNotNew() {
        isNew = false;
    }

}
