package com.sixt.service.test_vehicle_pool.domain.vehiclecreation;

import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.VehicleState;
import com.sixt.service.test_vehicle_pool.domain.vehicledeletion.VehicleCleaner;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static net.logstash.logback.marker.Markers.append;

@Component
public class FastlaneVehicleCreator {

    @Autowired
    private CommonRacVehicleCreator commonRacVehicleCreator;

    @Autowired
    private VehicleCleaner vehicleCleaner;

    private static final Logger LOGGER = LoggerFactory.getLogger(FastlaneVehicleCreator.class);

    public TestVehicle createFastlaneVehicleAutomation(int branchId, AcrissCode acrissCode, VehicleFuelType vehicleFuelType,
                                                       String vehiclePoolId) {
        LOGGER.info("Start creation of basic RAC vehicle for Fastlane for automation");
        BasicRacVehicle basicRacVehicle = commonRacVehicleCreator.createCommonRacVehicleBasic(VehicleType.FASTLANE_AUTOMATION,
                branchId, acrissCode, vehicleFuelType, true, false, false);
        LOGGER.info("Created basic RAC vehicle for Fastlane with id: {}", basicRacVehicle.getVehicleId());

        TestVehicle testVehicle = commonRacVehicleCreator.finalizeCommonRacVehicleCreation(basicRacVehicle, VehicleState.FASTLANE);

        if (testVehicle != null) {
            commonRacVehicleCreator.addTelematicUnit(testVehicle.getStaticVehicleData().getVehicleId(), testVehicle.getStaticVehicleData().getBasic().getVin());
            testVehicle.setVehicleType(VehicleType.FASTLANE_AUTOMATION);
            testVehicle.setVehiclePoolId(vehiclePoolId);
            LOGGER.info("Created automation Fastlane vehicle: {}", testVehicle.toString());
        } else {
            // If test vehicle could not be created successfully delete it from GoOrange / Sixt Classic
            vehicleCleaner.deleteVehicle(VehicleType.FASTLANE_AUTOMATION, basicRacVehicle.getVehicleId());
            LOGGER.info(append("vehicleId", basicRacVehicle.getVehicleId()), "Deleted not successfully added vehicle from GoOrange and Sixt Classic");
        }

        return testVehicle;
    }

    public BasicRacVehicle createFastlaneVehicleManualBasic(int branchId, AcrissCode acrissCode, VehicleFuelType vehicleFuelType,
                                                            boolean withWinterTyres, boolean fourWheels, boolean trailerCoupling) {
        return commonRacVehicleCreator.createCommonRacVehicleBasic(VehicleType.FASTLANE_MANUAL, branchId, acrissCode, vehicleFuelType,
                withWinterTyres, fourWheels, trailerCoupling);
    }

    public TestVehicle finishCreateFastlaneVehicleManual(BasicRacVehicle basicRacVehicle) {
        TestVehicle testVehicle = commonRacVehicleCreator.finalizeCommonRacVehicleCreation(basicRacVehicle, VehicleState.FASTLANE);

        if (testVehicle != null) {
            commonRacVehicleCreator.addTelematicUnit(testVehicle.getStaticVehicleData().getVehicleId(), testVehicle.getStaticVehicleData().getBasic().getVin());
            testVehicle.setVehicleType(VehicleType.FASTLANE_MANUAL);
            LOGGER.info("Created manual Fastlane vehicle: {}", testVehicle.toString());
        } else {
            // If test vehicle could not be created successfully delete it from GoOrange / Sixt Classic
            vehicleCleaner.deleteVehicle(VehicleType.FASTLANE_MANUAL, basicRacVehicle.getVehicleId());
            LOGGER.info(append("vehicleId", basicRacVehicle.getVehicleId()), "Deleted not successfully added vehicle from GoOrange and Sixt Classic");
        }

        return testVehicle;
    }

}
