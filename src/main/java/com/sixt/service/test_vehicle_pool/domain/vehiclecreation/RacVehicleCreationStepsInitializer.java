package com.sixt.service.test_vehicle_pool.domain.vehiclecreation;

import com.sixt.testdatamanagement.rac.vehicle.creation.config.Credentials;
import com.sixt.testdatamanagement.rac.vehicle.creation.config.TestEnvironment;
import com.sixt.testdatamanagement.rac.vehicle.creation.steps.RacVehicleCreationSteps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RacVehicleCreationStepsInitializer implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(RacVehicleCreationStepsInitializer.class);

    private RacVehicleCreationSteps racVehicleCreationSteps;

    private TestEnvironment testEnvironment;

    @Value("${environment.env}")
    private String environment;

    @Value("${app.ldap.user}")
    private String ldapUser;

    @Value("${app.ldap.password}")
    private String ldapPassword;

    @Override
    public void afterPropertiesSet() {
        Credentials credentials = new Credentials(ldapUser, ldapPassword);

        switch (environment) {
            case "dev":
                testEnvironment = TestEnvironment.ACCEPTANCE;
                racVehicleCreationSteps = new RacVehicleCreationSteps(testEnvironment, credentials);
                break;
            case "stage":
                testEnvironment = TestEnvironment.STAGE;
                racVehicleCreationSteps = new RacVehicleCreationSteps(testEnvironment, credentials);
                break;
            default:
                LOGGER.error("Not supported test environment <{}>", environment);
        }

        LOGGER.info("Running on environment: {}", testEnvironment.name());

        if (racVehicleCreationSteps != null) {
            racVehicleCreationSteps.logIn();
        }
    }

    public RacVehicleCreationSteps getRacVehicleCreationSteps() {
        return racVehicleCreationSteps;
    }

    public TestEnvironment getTestEnvironment() {
        return testEnvironment;
    }

}