package com.sixt.service.test_vehicle_pool.domain;

public class UndefinedTestCaseIdException extends RuntimeException {

	public UndefinedTestCaseIdException() {
			super("Undefined testcase id");
	}
}
