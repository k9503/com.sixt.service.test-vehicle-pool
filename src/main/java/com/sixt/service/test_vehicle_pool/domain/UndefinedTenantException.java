package com.sixt.service.test_vehicle_pool.domain;

public class UndefinedTenantException extends RuntimeException {

	public UndefinedTenantException() {
			super("Undefined tenant");
	}
}
