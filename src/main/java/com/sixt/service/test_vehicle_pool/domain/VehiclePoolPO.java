package com.sixt.service.test_vehicle_pool.domain;


import com.sixt.service.test_vehicle_pool.api.*;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;

@Entity
public class VehiclePoolPO implements Persistable<String> {

    @Id
    private String id;

    @Transient
    private boolean isNew = false;

    @Version
    private long version;

    private String tenant;

    private String vehicleType;

    private int branchId;

    private String acrissCode;

    private int poolSize;

    private int cleanAfterDays;

    private float centralPositionLat;

    private float centralPositionLon;

    private String vehicleFuelType;

    private VehiclePoolPO() {
    } // JPA


    private VehiclePoolPO(String id) {
        this.id = id;
        isNew = true;
    }

    public static VehiclePoolPO createNew(String id) {
        return new VehiclePoolPO(id);
    }

    @Override
    public String getId() {
        return id;
    }

    public void newPool(VehiclePool vehiclePool) {
        vehicleType = vehiclePool.getVehicleType().name();
        tenant = vehiclePool.getTenant().name();
        branchId = vehiclePool.getBranchId();
        acrissCode = vehiclePool.getAcrissCode();
        poolSize = vehiclePool.getPoolSize();
        cleanAfterDays = vehiclePool.getCleanAfterDays();
        centralPositionLat = vehiclePool.getCentralPosition().getLat();
        centralPositionLon = vehiclePool.getCentralPosition().getLon();
        vehicleFuelType = vehiclePool.getFuelType().name();
    }

    public void updatePool(VehiclePool vehiclePool) {
        tenant = vehiclePool.getTenant().name();
        branchId = vehiclePool.getBranchId();
        acrissCode = vehiclePool.getAcrissCode();
        poolSize = vehiclePool.getPoolSize();
        cleanAfterDays = vehiclePool.getCleanAfterDays();
        vehicleType = vehiclePool.getVehicleType().name();
        centralPositionLat = vehiclePool.getCentralPosition().getLat();
        centralPositionLon = vehiclePool.getCentralPosition().getLon();
        vehicleFuelType = vehiclePool.getFuelType().name();
    }

    public VehicleType getVehicleType() {
        return VehicleType.valueOf(vehicleType);
    }

    public int getBranchId() {
        return branchId;
    }

    public String getAcrissCode() {
        return acrissCode;
    }

    public String getTenant() {
        return tenant;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public int getCleanAfterDays() {
        return cleanAfterDays;
    }

    public VehicleFuelType getVehicleFuelType() {
        return VehicleFuelType.valueOf(vehicleFuelType);
    }

    public CentralPosition getCentralPosition() {
        return CentralPosition.newBuilder()
                .setLat(centralPositionLat)
                .setLon(centralPositionLon).build();
    }

    public VehiclePool getVehiclePool() {
        CentralPosition centralPosition = CentralPosition.newBuilder()
                .setLat(centralPositionLat)
                .setLon(centralPositionLon).build();

        return VehiclePool.newBuilder()
                .setId(id)
                .setVehicleType(VehicleType.valueOf(vehicleType))
                .setTenant(Tenant.valueOf(tenant))
                .setBranchId(branchId)
                .setAcrissCode(acrissCode)
                .setPoolSize(poolSize)
                .setCleanAfterDays(cleanAfterDays)
                .setCentralPosition(centralPosition)
                .setFuelType(VehicleFuelType.valueOf(vehicleFuelType)).build();
    }

    // Entity state handling - since we use a assigned (never null) identifier, we need to use Persistable
    // to tell the Spring JPARepository that the instance is new (i.e. has not been persisted before).
    // c.f. https://docs.spring.io/spring-data/jpa/docs/2.2.0.RELEASE/reference/html/#jpa.entity-persistence
    @Override
    public boolean isNew() {
        return isNew;
    }

    @PrePersist
    @PostLoad
    void markNotNew() {
        isNew = false;
    }

}
