package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.branch.api.BranchOuterClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;


@Component
public class BranchServiceProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(BranchServiceProxy.class);

    private RpcClient<BranchOuterClass.BranchGetResponse> getRpcClient;

    @Autowired
    public BranchServiceProxy(RpcClient<BranchOuterClass.BranchGetResponse> getRpcClient) {
        this.getRpcClient = getRpcClient;
    }

    public BranchOuterClass.BranchObject get(int branchId) {
        BranchOuterClass.BranchGetRequest request = BranchOuterClass.BranchGetRequest.newBuilder()
                .setBranchId(branchId)
                .build();

        try {
            LOGGER.debug("Calling branch service get for branch id {}", branchId);
            BranchOuterClass.BranchGetResponse branchGetResponse = getRpcClient.callSynchronous(request, new OrangeContext());
            return branchGetResponse.getBranch();
        } catch (RpcCallException e) {
            LOGGER.error("Failed to get branch data", e);
            return null;
        }
    }

}
