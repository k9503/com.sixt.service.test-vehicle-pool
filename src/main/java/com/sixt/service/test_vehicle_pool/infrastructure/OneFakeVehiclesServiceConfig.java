package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.one_fake_vehicles.api.ChangeVehicleStateResponse;
import com.sixt.service.one_fake_vehicles.api.GetVehicleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClientFactory;

@Configuration
public class OneFakeVehiclesServiceConfig {

    private static final String SERVICE_NAME = "com.sixt.service.one-fake-vehicles";
    private static final String METHOD_NAME_GET_VEHICLE_BY_VIN = "OneFakeVehicles.GetVehicleByVin";
    private static final String METHOD_NAME_CHANGE_VEHICLE_STATE = "OneFakeVehicles.ChangeVehicleState";

    @Value("${one-fake-vehicles.rpc-timeout-ms}")
    private int timeoutMs;

    @Value("${one-fake-vehicles.rpc-retries}")
    private int retries;

    @Autowired
    private RpcClientFactory rpcClientFactory;

    @Bean
    public RpcClient<GetVehicleResponse> getVehicleByVinRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_GET_VEHICLE_BY_VIN, GetVehicleResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

    @Bean
    public RpcClient<ChangeVehicleStateResponse> changeVehicleStateRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_CHANGE_VEHICLE_STATE, ChangeVehicleStateResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

}
