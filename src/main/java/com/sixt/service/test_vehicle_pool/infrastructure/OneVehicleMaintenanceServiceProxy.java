package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.one_vehicle_maintenance.api.ReleaseResponse;
import com.sixt.service.one_vehicle_maintenance.api.ReleaseVehicleRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import static net.logstash.logback.marker.Markers.append;


@Component
public class OneVehicleMaintenanceServiceProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(OneVehicleMaintenanceServiceProxy.class);

    private RpcClient<ReleaseResponse> releaseVehicleRpcClient;

    @Autowired
    public OneVehicleMaintenanceServiceProxy(RpcClient<ReleaseResponse> releaseVehicleRpcClient) {
        this.releaseVehicleRpcClient = releaseVehicleRpcClient;
    }

    public void releaseVehicle(String vehicleId) {
        ReleaseVehicleRequest request = ReleaseVehicleRequest.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        try {
            LOGGER.debug(append("vehicleId", vehicleId), "Calling one-vehicle-maintenance service release vehicle");
            releaseVehicleRpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException e) {
            LOGGER.error(append("vehicleId", vehicleId), "Failed to release vehicle", e);
        }
    }

}
