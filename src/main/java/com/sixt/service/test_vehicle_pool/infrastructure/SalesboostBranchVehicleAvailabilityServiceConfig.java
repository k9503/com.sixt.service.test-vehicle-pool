package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.salesboost_branch_vehicle_availability.api.VehicleDetailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClientFactory;

@Configuration
public class SalesboostBranchVehicleAvailabilityServiceConfig {

    private static final String SERVICE_NAME = "com.sixt.service.salesboost-branch-vehicle-availability";
    private static final String METHOD_NAME_GET = "SalesboostVehicleAvailabilityService.GetStatusByVehicleID";


    @Value("${salesboost-branch-vehicle-availability.rpc-timeout-ms}")
    private int timeoutMs;

    @Value("${salesboost-branch-vehicle-availability.rpc-retries}")
    private int retries;

    @Autowired
    private RpcClientFactory rpcClientFactory;

    @Bean
    public RpcClient<VehicleDetailResponse> getStatusByVehicleIdRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_GET, VehicleDetailResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

}
