package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.journey.api.JourneyOuterClass.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import static net.logstash.logback.marker.Markers.append;


@Component
public class JourneyServiceProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(JourneyServiceProxy.class);

    private RpcClient<GetVehicleJourneyResponse> getVehicleJourneyRpcClient;

    private RpcClient<ForceEndJourneyResponse> forceEndJourneyRpcClient;

    @Autowired
    public JourneyServiceProxy(RpcClient<GetVehicleJourneyResponse> getVehicleJourneyRpcClient,
                               RpcClient<ForceEndJourneyResponse> forceEndJourneyRpcClient) {
        this.getVehicleJourneyRpcClient = getVehicleJourneyRpcClient;
        this.forceEndJourneyRpcClient = forceEndJourneyRpcClient;
    }

    public GetVehicleJourneyResponse getVehicleJourney(String vehicleId) {
        GetVehicleJourneyQuery request = GetVehicleJourneyQuery.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        try {
            LOGGER.debug(append("vehicleId", vehicleId), "Calling journey service get vehicle journey");
            return getVehicleJourneyRpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException rpcCallException) {
            if (!rpcCallException.getErrorCode().equals(GetVehicleJourneyResponse.Error.NO_CURRENT_JOURNEY.toString())) {
                LOGGER.error(append("vehicleId", vehicleId), "Failed to get vehicle journey", rpcCallException);
            }

            return null;
        }
    }

    public void forceEndJourney(String journeyId) {
        ForceEndJourneyCommand request = ForceEndJourneyCommand.newBuilder()
                .setJourneyId(journeyId)
                .build();

        try {
            LOGGER.debug("Calling journey service force end journey {}", journeyId);
            forceEndJourneyRpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException e) {
            LOGGER.error("Failed to force end journey", e);
        }
    }

}