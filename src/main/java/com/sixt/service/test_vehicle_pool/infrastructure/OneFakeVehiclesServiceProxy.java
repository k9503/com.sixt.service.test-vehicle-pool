package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.one_fake_vehicles.api.*;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.utils.Sleeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import static net.logstash.logback.marker.Markers.append;


@Component
public class OneFakeVehiclesServiceProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(OneFakeVehiclesServiceProxy.class);

    private RpcClient<GetVehicleResponse> getVehicleByVinRpcClient;
    private RpcClient<ChangeVehicleStateResponse> changeVehicleStateRpcClient;

    @Autowired
    public OneFakeVehiclesServiceProxy(RpcClient<GetVehicleResponse> getVehicleByVinRpcClient,
                                       RpcClient<ChangeVehicleStateResponse> changeVehicleStateRpcClient) {
        this.getVehicleByVinRpcClient = getVehicleByVinRpcClient;
        this.changeVehicleStateRpcClient = changeVehicleStateRpcClient;
    }

    public Vehicle getVehicleByVin(String vin) {
        GetVehicleByVinQuery request = GetVehicleByVinQuery.newBuilder()
                .setVin(vin)
                .build();

        try {
            LOGGER.debug("Calling one-fake-vehicles service getVehicleByVin for vin {}", vin);
            GetVehicleResponse getVehicleResponse = getVehicleByVinRpcClient.callSynchronous(request, new OrangeContext());
            return getVehicleResponse.getVehicle();
        } catch (RpcCallException e) {
            LOGGER.debug("No vehicle with VIN {} found", vin);
            return null;
        }

    }

    public void setState(TestVehicle testVehicle) throws RpcCallException {
        String vin = testVehicle.getStaticVehicleData().getBasic().getVin();
        Vehicle fakeVehicle = getVehicleByVin(vin);

        VehicleState.Position position = fakeVehicle.getVehicleState().getPosition();
        position = position.toBuilder()
                .setLatitude(testVehicle.getDynamicVehicleData().getPosition().getLatitude())
                .setLongitude(testVehicle.getDynamicVehicleData().getPosition().getLongitude()).build();

        VehicleState vehicleState = fakeVehicle.getVehicleState();
        vehicleState = vehicleState.toBuilder()
                .setPosition(position)
                .setIgnition(Ignition.valueOf(testVehicle.getDynamicVehicleData().getIgnitionState()))
                .setFuelLevel(testVehicle.getDynamicVehicleData().getFuelLevelTotal())
                .setChargeLevel(testVehicle.getDynamicVehicleData().getChargeLevel())
                .setOdometer(testVehicle.getDynamicVehicleData().getOdometer()).build();

        ChangeVehicleStateCommand request = ChangeVehicleStateCommand.newBuilder()
                .setVin(vin)
                .setVehicleState(vehicleState)
                .setSkipFireEvent(false)
                .build();

        LOGGER.debug(append("vehicleId", testVehicle.getStaticVehicleData().getVehicleId()),
                "Calling one-fake-vehicles service setState and setting new state: {}", vehicleState.toString());
        changeVehicleStateRpcClient.callSynchronous(request, new OrangeContext());

        Sleeper.sleepForSeconds(10);
    }

}
