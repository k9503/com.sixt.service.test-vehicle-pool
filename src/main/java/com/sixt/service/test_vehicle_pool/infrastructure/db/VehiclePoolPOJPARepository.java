package com.sixt.service.test_vehicle_pool.infrastructure.db;

import com.sixt.service.test_vehicle_pool.domain.VehiclePoolPO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VehiclePoolPOJPARepository extends JpaRepository<VehiclePoolPO, String> {

    Optional<VehiclePoolPO> findByTenantAndVehicleType(String tenant, String vehicleType);

}