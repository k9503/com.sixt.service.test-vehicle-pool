package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.salesboost_branch_vehicle_availability.api.GetVehicleByVehicleIDRequest;
import com.sixt.service.salesboost_branch_vehicle_availability.api.VehicleDetailResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;


@Component
public class SalesboostBranchVehicleAvailabilityServiceProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(SalesboostBranchVehicleAvailabilityServiceProxy.class);

    private RpcClient<VehicleDetailResponse> getStatusByVehicleIdRpcClient;

    @Autowired
    public SalesboostBranchVehicleAvailabilityServiceProxy(RpcClient<VehicleDetailResponse> getStatusByVehicleIdRpcClient) {
        this.getStatusByVehicleIdRpcClient = getStatusByVehicleIdRpcClient;
    }

    public VehicleDetailResponse getStatusByVehicleId(String vehicleId) {
        GetVehicleByVehicleIDRequest request = GetVehicleByVehicleIDRequest.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        try {
            LOGGER.debug("Calling salesboost-branch-vehicle-availability service get status by vehicle id {}", vehicleId);
            VehicleDetailResponse vehicleDetailResponse = getStatusByVehicleIdRpcClient.callSynchronous(request, new OrangeContext());
            return vehicleDetailResponse;
        } catch (RpcCallException e) {
            LOGGER.error("Failed to get branch vehicle availability status", e);
            return null;
        }
    }

}
