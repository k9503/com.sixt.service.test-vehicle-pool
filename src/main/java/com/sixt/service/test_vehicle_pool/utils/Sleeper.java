package com.sixt.service.test_vehicle_pool.utils;

import org.slf4j.LoggerFactory;

public class Sleeper {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Sleeper.class);

    public static void sleepForSeconds(int seconds) {
        LOGGER.debug("Sleeping now for {} seconds", seconds);
        sleepNoException(seconds * 1000);
    }

    private static void sleepNoException(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            // NoOp
        }
    }

}
