package com.sixt.service.test_vehicle_pool.utils;

import java.util.UUID;

public class UUIDCreator {
    public static String createRandomE2EUUID(){
        return "e2e" + UUID.randomUUID().toString().substring("e2e".length());
    }
}
